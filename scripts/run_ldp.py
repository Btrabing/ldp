#!/usr/bin/env python
#
# Script used to run the LDP and create figures
#
# Written June 19, 2021
# Ben Trabing, btrabing@colostate.edu
#
#
#---------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
import matplotlib.path as mpath
import matplotlib.dates as md
from matplotlib.dates import DateFormatter
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
import glob
import subprocess
import sys
import warnings
warnings.filterwarnings("ignore")
#------------------#
# Read in custom functions we have defined

from ldp_main import *
from ldp_util import *
from Plot_util import *
from ldp_proto import *

#-------------------------------------------
# Set Parameters Here                     
#-------------------------------------------
Make_Figures = True              # If True then figures will be created
Fig_Loc      = '../output'       # Where to save the figures
TXT_Loc      = '../output'       # Where to save the text files
MCRALL_Path  = '../data/mcrall/' # Path to the WSP model files
REALTIME     = False             # If True then only recent mcrall files will be run
STMID        = 'al092021'        # Only used if REALTIME=False:  ATCF identifier (al052019). Use * to identify groups of storms/years
MMDDHH       = '*'               # Only used if REALTIME=False:  use * to get all times for given storms/years. Otherwise use MMDDHH format as a string

#---------------------------------------------
# Run Landfall Intensity Distribution Code                     
#---------------------------------------------

if __name__ == '__main__':
    
    if REALTIME==False: #Mcrall files to run on in offline mode
        files_to_test = sorted(glob.glob((MCRALL_Path+'/mcrall_'+STMID+'_'+MMDDHH+'.dat')))

    elif REALTIME==True: # in realtime, use "get_recent_file" to identify recently generated mcrall files
        files_to_test = sorted(get_recent_file(MCRALL_Path+'/mcrall_*'))

####################

    #Read in the distance to land file
    landpath = '../data/gdland_table.dat'
    gdland = read_gdland(landpath)    
    
    #Loop through the files that Code should be run on
    for filenames in files_to_test:
        STORMNAME = filenames.split('_')[-2]
        DTOFAN = STORMNAME[-4:]+(filenames.split('_')[-1])[:-4]

        if REALTIME==True:
            #Open a log file with the name of the storm/time
            log_file = open(("../log/"+STORMNAME+"_"+DTOFAN+".log"),"w")
            sys.stdout = log_file


        try:
            #Create the Output Txt file with landfall probabilities
            state_landfall_probs(filenames,gdland,TXT_Loc)
            #Save the name of the output file to generate the figure off of
            OUTFILE_NAME = TXT_Loc+'/WSPLF_'+STORMNAME.upper()+'_'+DTOFAN+'_Probs.txt'

        except:
            print('Could not run LDP')
            Plot_empty_ldp(filenames,Fig_Loc)
            continue

        #If we want to make the figures (
        if Make_Figures:           

            #Plot the landfall prototype graphic
            Plot_Ptype_fromtxt(OUTFILE_NAME,mcrall_path=filenames,GRAPHIC='1',TABLE='1',Output_Loc=Fig_Loc)
            Plot_Ptype_fromtxt(OUTFILE_NAME,mcrall_path=filenames,GRAPHIC='1',TABLE='2',Output_Loc=Fig_Loc)
            
            #Plot Other Graphics
            Plot_VMAX50_fromtxt(OUTFILE_NAME,mcrall_path=filenames,Output_Loc=Fig_Loc)
            Plot_VMAX90_fromtxt(OUTFILE_NAME,mcrall_path=filenames,Output_Loc=Fig_Loc)

        if REALTIME==True:
            #Close the log file
            log_file.close()
