#!/usr/bin/env python
#--------------------------------------------------------
#
# This script has the main functions that use the util
# and plotting scripts to make the landfall distributions
# graphics and table
# 
# Written June 18, 2021
# Ben Trabing, btrabing@colostate.edu
#
'''
state_landfall_probs -> creates the output table
(Outdated) Plot_Region_Evo -> creates the figure with the state level probability forecast

'''
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
import matplotlib.path as mpath
import matplotlib.dates as md
from matplotlib.dates import DateFormatter
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr

#------------------#

from ldp_util import *
from Plot_util import *
from ldp_proto import OFCL_Intensity_

#------------------#

def state_landfall_probs(filepath,gdland,Output_Loc):
    '''
    Main function to compute landfall probabilitiies and output a text file
    Inputs: mcrall filepath, distance to land field, and output location

    Output: txt file with probabilities



    '''

    #####################################################    
    # Bias correction fixes most likely intensity to OFCL
    # last over-ocean intensity
    #  
    apply_bc = True

    if apply_bc==True:
        OFCL_H, OFCL_I = OFCL_Intensity_(filepath,gdland)


    STORMNAME = filepath.split('_')[-2]
    BASIN = STORMNAME[:2]
    DTofAn = STORMNAME[-4:]+(filepath.split('_')[-1])[:-4]
#####################################################    
    print('Processing %s at %s'%(STORMNAME,DTofAn))
#####################################################

    data = read_mcrall(filepath) #This has all of the data
    D2L_var = is_land(data['lon'],data['lat'],gdland) #This is the distance to land for each point
    
    data_k = get_all_landfalls(filepath,gdland) #This is all the landfall points identified
    all_int = data_k[3,:]
    all_hour = data_k[0,:]
    all_ensn = data_k[4,:]
    all_rmw = data_k[5,:]
    all_lat = data_k[1,:]
    all_lon = data_k[2,:]


    #Read in the state geometry and data
    SNAMES,SGEOGS = get_region_shapes(BASIN)
    
#####################################################

####################################################
    HOURS = data['tau'].values
    ensno = data.index
            
    if data_k.shape[1]==0:
        #There are no landfalls
        return
     

    #Now we will plot the landfalls

    land_states = []
    land_vmax = []
    land_hour = []
    land_ensn = []
    land_lat = []
    land_lon = []

    for i_ in np.arange(0,len(data_k[0,:])):
        
        lon_ = all_lon[i_]-360
        lat_ = all_lat[i_]
        rmw_ = all_rmw[i_] / 60. #convert rmw from n m to degrees (correct later)
#        rmw_ = 1.

        
        
        lpoint = Point((lon_,lat_))
        lpoint_buffer = lpoint.buffer(float(rmw_))

        # Loop through the state shape files
        for state,geom in zip(SNAMES,SGEOGS):
            #If the landfalls intersect any of the state shapefiles then save to lists
            if (geom.intersects(lpoint_buffer)):
                land_states.append(state)
                land_vmax.append(all_int[i_])
                land_hour.append(int(all_hour[i_]))
                land_ensn.append(all_ensn[i_])
                land_lat.append(lat_)
                land_lon.append(lon_+360.)


    land_states= np.array(land_states)
    land_vmax= np.array(land_vmax)
    land_hour= np.array(land_hour)
    land_ensn= np.array(land_ensn)
    land_lon = np.array(land_lon)
    land_lat = np.array(land_lat)
    land_cats= wsp_to_SScat(land_vmax)


    states_toplot = rorder_states(np.unique(land_states),BASIN)
    
    #These variables are defined to simplify output strings of zero probability states
    No_land_ = ['   0.0']*Config_.Forecast_Days
    Small_land_ = ['  <%i.0'%Config_.Minimum_Perc]*Config_.Forecast_Days
    No_int = [0,0,0,0,0,0,0,0,0,0,0]  #This includes the no mean or percentiles for intensity and landfall time
    No_int_ = [format("%03.1f"%el, '>6') for el in No_int] 
    Small_int_ = No_int_ #This is automatically defined to apply to each forecast hour

    #use the keys from the confile dictionary
    mcdict,ordict,abrdict,tzdict = rcdict(BASIN)
    All_states = list(mcdict.keys())

    DATA_TO_OUTPUT = []
    
    for state_ in All_states:       
        statel_cats= land_cats[land_states==state_]        
        statel_vmax= land_vmax[land_states==state_]
        statel_hour= land_hour[land_states==state_]
        statel_ensn= land_ensn[land_states==state_]
        statel_lon= land_lon[land_states==state_]
        statel_lat= land_lat[land_states==state_]
        
        if ((statel_ensn.size==0)):
            DATA_TO_OUTPUT.append(([format(state_, '<23')]+No_land_+No_int_))
            continue
            

                    
        #If there is not more than 2% of ensembles with a landfall in the region then skip it
        if ((100.*(len(np.unique(statel_ensn))/1000.)<=Config_.Minimum_Perc)): #if there is less than 2% of ensemble members
            DATA_TO_OUTPUT.append(([format(state_, '<23')]+Small_land_+Small_int_))
            continue

        # Calculate the data percentiles assuming there are more than the minimum
        else:
            #Get the percentiles, and median for forecast timing and intensity
            vmaxperc = int(np.nanpercentile(statel_vmax,90))
            vmaxmean = int(np.nanpercentile(statel_vmax,50))
            hourperc = int(np.nanpercentile(statel_hour,10))
            hourmean = int(np.nanpercentile(statel_hour,50))

            if apply_bc==True:
                vmaxmean_old = vmaxmean
                vmaxperc_old = vmaxperc

                #Fixed percentile increase
#                if hourmean<=48.:
#                    BC_val = 1.15
#                elif hourmean>48:
#                    BC_val = 1.20

                #Bias correct using NHC Forecast
                BC_val = OFCL_I[int(hourmean)-1]-vmaxmean

                 
                statel_vmax = BC_val+statel_vmax
                statel_cats = wsp_to_SScat(statel_vmax)
                vmaxperc = int(np.nanpercentile(statel_vmax,90))
                vmaxmean = int(np.nanpercentile(statel_vmax,50))
                print('Upping Median from %i to %i'%(int(vmaxmean_old),int(vmaxmean))) 
                print('Upping Max Likely from %i to %i'%(int(vmaxperc_old),int(vmaxperc)))


            #Get the unique number of ensemble members with their index
            unens,unens_i = np.unique(statel_ensn,return_index=True)

            #Use a list comprehension to get probabilities over each forecast interval (24 hours)
            PROBS_OUT = [100.*len(unens[statel_hour[unens_i]<=int(hh)])/1000. for hh in np.arange(24,24*(Config_.Forecast_Days+1),24)]
            PROBS_OUT_S = [format("%03.1f"%el, '>6') for el in PROBS_OUT] #Format the above line as strings

            #Get the probability for storms being in category ranges
            pc_dcat= 100.*len(unens[statel_cats[unens_i]<=-1])/len(unens)     
            pc_scat= 100.*len(unens[statel_cats[unens_i]==0])/len(unens)     
            pc_1cat= 100.*len(unens[statel_cats[unens_i]==1])/len(unens)     
            pc_2cat= 100.*len(unens[statel_cats[unens_i]==2])/len(unens)     
            pc_3cat= 100.*len(unens[statel_cats[unens_i]==3])/len(unens)     
            pc_4cat= 100.*len(unens[statel_cats[unens_i]==4])/len(unens)     
            pc_5cat= 100.*len(unens[statel_cats[unens_i]==5])/len(unens)     


            #Add variables to a list
            PCCAT_OUT = [pc_dcat, pc_scat, pc_1cat, pc_2cat, pc_3cat, pc_4cat, pc_5cat, vmaxperc, vmaxmean, hourperc, hourmean]
            PCCAT_OUT_S = [format("%03.1f"%el, '>6') for el in PCCAT_OUT]  #Format all variables in above line as string



            DATA_TO_OUTPUT.append(([format(state_, '<23')]+PROBS_OUT_S+PCCAT_OUT_S)) #Save probs for this state into list


    #File name to output to
    Ouput_name = Output_Loc+'/'+'WSPLF_'+STORMNAME.upper() + '_'+DTofAn+'_Probs.txt'
    
    #Open the file to write everything to
    with open(Ouput_name, 'w') as fp:
        #----------------------
        # Pring first header
        Header_0 = [(' '+STORMNAME.upper()+'   '+DTofAn+' '),'Probability of Landfall (%)','    Percentage of Landfalls at Intensity (%)']
        Header_0 = '\t'.join(Header_0) #tab delimit header into right aligned columns
        fp.write(Header_0) #write to file
        fp.write('\n')        
        
        #----------------------
        #Adds the second header corresponding to columnar state/region data
        HOURS_F = [format("<%03ih"%el, '>6') for el in np.arange(24,24*(Config_.Forecast_Days+1),24)]
        INS_F = ['  <=TD', '    TS', '    H1', '    H2', '    H3', '    H4', '    H5']
        EXTR_L = ['90%_kt','Avg_kt',' 10%_h',' Avg_h']
        Header_1 = [format('State/Region ', '<23')] + HOURS_F+INS_F+EXTR_L  # list of header info
        Header_1 = '\t'.join(Header_1) #tab delimit header into right aligned columns
        fp.write(Header_1) #write to file
        fp.write('\n')   #Move to next line

        #----------------------
        #print probs for each state

        for i in np.arange(0,len(DATA_TO_OUTPUT)): #Loop through intensity change thresholds since more of them
            Row_ = DATA_TO_OUTPUT[i]
            Row = '\t'.join([el for el in Row_]) #tab delimit header into right aligned columns
            fp.write(Row)  #write to file
            fp.write('\n') #Move to next line


#------------------#
def Plot_Region_Evo(file_to_test,region,Output_Loc):
    # Obsoleve plotting function

    #Initialize the Plot
    fig = plt.figure(figsize=(12,7))
    ax = plt.subplot(111) #This is where the probabilities are plotted
    ax2 = ax.twiny().twinx() #This is the axis the intensity distribution is plotted on

    STORMNAME = file_to_test.split('_')[-3]
    BASIN = STORMNAME[:2]
    DTofAn = (file_to_test.split('_')[-2])
        
    ax.text(.5,1.015,region.upper(),fontsize=16,color='k',ha="center",va='bottom',transform=ax.transAxes, fontweight='bold',zorder=99999)  
    ax.text(.05,1.02,'Init: %s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d'),fontsize=16,color='k',ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.95,1.02,STORMNAME.upper(),fontsize=16,color='k',ha="right",va='bottom',transform=ax.transAxes,zorder=99999)

    #This is the formatting for the labels of the Saffif-Simpson intensity distribution
    CATS_S = ['$\leq$TD','TS','CAT 1','CAT 2','CAT 3','CAT 4','CAT 5']

    #These are the colors used for the different times (current, -6 h, -12 h, -18 h)
    Probcolor = ['orange','red','darkred','magenta']
    Timecolor = ['gray','lime','forestgreen','darkgreen']
    Timecolor = ['silver','silver','silver','darkgreen']
    markers_  = ['s','v','D','o']

    early_times = []
    mean_times  = []

    #Loop through the hours in order of oldest to newest
    for i,previous_h in enumerate([18,12,6,0]):  
        #this should be the last part of this loop in which we plot the current forecast
        if previous_h==0:
            #Read in the output file and format it
            TIMES, PROBS, PROBSD, CATSD, v_90, v_av, h_10, h_av  = format_LID_output(file_to_test,region,0)
            
            CTIME = TIMES[0]-dt.timedelta(hours=24)

            ax.plot(np.array(TIMES)-dt.timedelta(hours=24),PROBS,lw=4,c=Probcolor[i],marker=markers_[i],markersize=12,markeredgecolor='k',zorder=20)

#            ax.bar(np.array(TIMES)+dt.timedelta(hours=12),np.squeeze(PROBSD),dt.timedelta(hours=24),facecolor=Probcolor[i],alpha=.3,edgecolor='k')
            ax.bar(np.array(TIMES)-dt.timedelta(hours=12),np.squeeze(PROBSD),1,facecolor=Probcolor[i],alpha=.3,edgecolor='k',zorder=11)

            ax.axvline(CTIME,c=Probcolor[i],lw=3,ls='dashed')

            ax.axvspan(CTIME+dt.timedelta(hours=h_10),CTIME+dt.timedelta(hours=h_av),color=Timecolor[i],alpha=.5,zorder=10)
            early_times.append((CTIME+dt.timedelta(hours=h_10)))
            mean_times.append((CTIME+dt.timedelta(hours=h_av)))

            #Get the index of the mean saffir-simpson wind speed category (Add 1 because of <=TD)
            indofmean = int(wsp_to_SScat(np.squeeze(v_90)))+1

            #------------------------


            #bar plot of the intensity distribution
            ax2.barh(CATS_S,np.squeeze(CATSD),edgecolor='k')
            #Plot only the index of the mean as a magenta color
            ax2.barh(CATS_S[indofmean],np.squeeze(CATSD)[indofmean],facecolor='magenta',edgecolor='k')


            #Now loop through the intensity categories and label the percentages
            for i, v in enumerate(np.squeeze(CATSD)):
                #if the index matches that of the mean, then make that text bold to highlight it
                if i==indofmean:
                    ax2.text(v, i, str(v)+'%-', color='k',ha="right",va='center', fontsize=14, fontweight='bold')
                else:
                    ax2.text(v, i, str(v)+'%-', color='k',ha="right",va='center', fontsize=14)


            #These will be used for the limits of the plot
            startt = CTIME-dt.timedelta(hours=24)
            finalt = TIMES[-1]+dt.timedelta(hours=36) #added buffer time for itensity distribution


            #------------------------



        else:
            #We use a try/except block because there may or may not be older forecasts that we can display
            try:
                TIMESp, PROBSp, PROBSDp, CATSDp, v_90p, v_avp, h_10p, h_avp  = format_LID_output(file_to_test,region,previous_h)

                CTIMEp = TIMESp[0]-dt.timedelta(hours=24)

                ax.plot(np.array(TIMESp)-dt.timedelta(hours=24),PROBSp,lw=3,c=Probcolor[i],marker=markers_[i],markersize=10,markeredgecolor=Probcolor[i],zorder=12)
                ax.axvline(CTIMEp,c=Probcolor[i],lw=3,ls='dashed')

                if int(h_10p)>0:
                    early_times.append((CTIMEp+dt.timedelta(hours=h_10p)))
                    mean_times.append((CTIMEp+dt.timedelta(hours=h_avp)))

#                ax.axvspan(CTIMEp+dt.timedelta(hours=h_10p),CTIMEp+dt.timedelta(hours=h_avp),color=Timecolor[i],alpha=.5)
            except:
                print('Could not find the previous %s h forecast from %s'%(previous_h,DTofAn))

    if len(early_times)==1:
        ax.text(early_times[0],50,'Earliest Likely Landfall Time',ha='right',va='center',fontsize='large',backgroundcolor='None',rotation=90)
        ax.text(mean_times[0]+dt.timedelta(hours=1),50,'Most Likely Landfall Time',ha='left',va='center',fontsize='large',backgroundcolor='None',rotation=90)


    if len(early_times)>1: # only shade earlist if there are older forecast times
        ax.axvspan(min(early_times),max(mean_times),color='gray',alpha=.5,zorder=9)
        ax.text(min(early_times),50,'Earliest Likely Landfall Time',ha='right',va='center',fontsize='large',backgroundcolor='None',rotation=90)
        ax.text(max(mean_times)+dt.timedelta(hours=1),50,'Most Likely Landfall Time',ha='left',va='center',fontsize='large',backgroundcolor='None',rotation=90)

    #----------------------------------------------
    # Format the figure axis
    
    # Set the limits, which has extra times to account for histogram of intensities
    ax.set_xlim(startt,finalt)
    ax.set_ylim(0,100)
    ax.set_yticks(np.arange(0,110,10))
    ax.set_yticklabels(np.arange(0,110,10))


    #Format the size of the numbers along axis
    ax.tick_params(axis='x',labelsize=12)
    ax.tick_params(axis='y',labelsize=14)
    ax2.tick_params(axis='y',labelsize=14)

    #Make sure that the Saffir-Simpson bar plots or on the right side
    ax2.invert_xaxis()
    ax2.set_xlim(500,0) #Increase 500 to scale the length of the intensity probabilities

    #Make sure the saffir simpson histogram does not make ticks on top/bottom
    ax2.tick_params(axis='x',top='off', bottom='off', labelbottom='off', labeltop='off')
    ax2.set_xticks([])

    ax.set_ylabel('Cumulative Probability of Landfall (%)',fontsize=16)
    ax2.set_ylabel('Saffir-Simpson Category at Landfall',fontsize=16)

    #Set major and minor ticks
    ax.xaxis.set_major_locator(md.HourLocator(interval=12))
    ax.xaxis.set_minor_locator(md.HourLocator(interval=6))

    ax.xaxis.set_major_formatter(md.DateFormatter('%d %b\n%HZ'))


    #---------------------------------
    #Display a legend below the plot?
    # Legend needs some development
#     ax.plot([-1],[-1],lw=4,c=Probcolor[3],marker=markers_[3],markersize=12,markeredgecolor='k',label='Cumulative Probability of Landfall for Current Forecast')
#     ax.plot([-1],[-1],lw=4,c=Probcolor[2],marker=markers_[2],markersize=10,markeredgecolor=Probcolor[2],label='Cumulative Probability of Landfall for 6h old Forecast')
#     ax.plot([-1],[-1],lw=4,c=Probcolor[1],marker=markers_[1],markersize=10,markeredgecolor=Probcolor[1],label='Cumulative Probability of Landfall for 12h old Forecast')
#     ax.plot([-1],[-1],lw=4,c=Probcolor[0],marker=markers_[0],markersize=10,markeredgecolor=Probcolor[0],label='Cumulative Probability of Landfall for 18h old Forecast')
#     ax.bar([-1],[-1],facecolor=Probcolor[3],alpha=.3,edgecolor='k',label='Probability of Landfall within 24 h')
#     ax.axvline(-1,c='gray',lw=3,ls='dashed',label='WSP Model Initialization Time')
#     ax.axvspan(-10,-1,color=Timecolor[-1],alpha=.5,label='10th Percentile to Mean Landfall Time')    
#     ax.barh([-1],[-1],facecolor='C0',edgecolor='k',label='Percentage of Intensity at Landfall')
#     ax.barh([-1],[-1],facecolor='magenta',edgecolor='k',label='Mean Intensity at Landfall')

#     ax.legend(loc='lower left',ncol=2,prop={'size':12},bbox_to_anchor=(0, -0.4))
    #---------------------------------
    
    plt.savefig((Output_Loc + '/' + 'LID_' + region.replace(" ", "") + '_' + STORMNAME.upper() + '_' + DTofAn + '.png'),bbox_inches='tight',pad_inches=0)

    plt.close(fig)    
#    plt.show()


