#!/usr/bin/env python
#---------------------------------
import pandas as pd
import xarray as xr
import numpy as np
import datetime as dt
import subprocess

#------------------#
class Config_:
    #Read in the namelist file
    config_file = np.genfromtxt('../config/Namelist', dtype=str)
    
    for line in config_file: #Go through the file and execute the commands
        exec(''.join(line)) #What this does is essentially creat a global variable that is found when importing this code

def get_nhc_errors(BASIN):
    # This function read in the file below which should have the most recent 5 year errors
    path_to_errors = '../data/NHC_Radii.txt'

    df = pd.read_csv(path_to_errors,names=['HOUR','AL','EP','CP'],header=None,skiprows=1)#, delim_whitespace=True)

    # Return the basin specific error values
    return np.array(df[BASIN.upper()].values).astype(float)


def get_recent_file(mcrall_path):

    #Seach the location of the mcrall files (they are updated in realtime) for recent files
    output = subprocess.check_output(('find '+mcrall_path+' -mmin -360 -ls'),shell=True)

    #Decode the find output
    ffs = (output.decode('ascii').split("\n"))

    realt_path = []
    for ff in ffs: #Now loop over all the paths in the file
        date_ = ff.split(" ")[-1] #Split the datefile

        if len(date_)>10: #check that we dont have a random file that is small
            realt_path.append(date_)


    return realt_path #return all the file paths as a list

#------------------#
def read_mcrall(data_filepath):
    #This function reads in the mcrall file that has the MC realizations

    #Names of the mcrall columns
    names=['t=', 'tau', 'vmx', 'lat','lon','rmw', 'xt', 
           'r34_1','r34_2','r34_3','r34_4',
          'r50_1','r50_2','r50_3','r50_4',
          'r64_1','r64_2','r64_3','r64_4',
          'r100_1','r100_2','r100_3','r100_4',
          'spd','beta','apt','thp']

    df = pd.read_csv(data_filepath, names=names, header=None, skiprows=13, delim_whitespace=True)
    df.loc[:,'ens_no'] = (df['tau'] == 0).cumsum() #Sum the indexes to get ensemble number
    
    return df

#------------------#

def read_ofcl_mcrall(data_filepath):
    #This function reads in the mcrall file that has the MC realizations

    #Names of the mcrall columns
    names=['tau', 'lon', 'lat','vmx','nouse']

    df = pd.read_csv(data_filepath, names=names, header=None, skiprows=2, nrows=11, delim_whitespace=True)

    return df               
                

#------------------#
def read_gdland(filepath):
    #Read the gdland file (which has the distance to land points)

    with open(filepath) as f:
        header = next(f)
        data = pd.read_fwf(f,header=None, widths=[9,]*10)

    coords = header.split()
    lons = np.linspace(float(coords[0]), float(coords[1]), int(coords[3]))
    lats = np.linspace(float(coords[4]), float(coords[5]), int(coords[7]), endpoint=False)
    gdland = xr.DataArray(data=np.array(data).reshape(len(lats),len(lons)),
                coords={'lat':lats, 'lon':lons}, dims=('lat','lon'))

    gdland = gdland.fillna(0)
    return gdland

#------------------#
def is_land(lon,lat,landfile=None):
    # Identifies whether the provided lat, lon is overland
    # Returns distance to land along track
    if landfile is None: 
        #Read in gdland file if not provided
        #Use the expected location in the Data directory
        gdland = read_gdland('../data/gdland_table.dat') #Inefficient if calling thousands of time
    else:
        #Assume that the landfile is being provided
        gdland = landfile
    
    #Get lat/lon seperate
    lons = gdland['lon'].values
    lats = gdland['lat'].values

    d2l = gdland.values #get the distance to land variables
    
    
    #If an array of lons are provided then lets calculate the dtl for each point
    if len(lon)>1:
        d2ls=[]
        i=0
        for lat_,lon_ in zip(lat,lon):
            d2ls.append(d2l[np.argmin(np.abs(lats-lat_)),np.argmin(np.abs(lons-lon_))])
        return np.array(d2ls) #return the distance to land 
            
    else:   
        #Return the nearest defined distance to land point to the given lat/lon
        return d2l[np.argmin(np.abs(lats-lat)),np.argmin(np.abs(lons-lon))]


#------------------#
def get_all_landfalls(filepath,gdland):
    #Uses the mcrall file path and distance to land file to caluclate the landfalls in each ensemble member

    data = read_mcrall(filepath) #read mcrall file
    D2L_var = is_land(data['lon'],data['lat'],gdland) #read distance to land file and calculates distance to land for each lat/lon in mcrall file


    #Hours and ensemble member number
    HOURS = data['tau'].values
    ensno = data.index

    #initialize list of landfall attributes
    land_ints = []
    land_hour = []
    land_rmw = []
    land_lats = []
    land_lons = []
    land_ensn = []

    #now lets loop through each unique ensemble member id
    for ii in np.unique(ensno):
        #get data associated with just the ensemble member
        HOURS_ = HOURS[ensno==ii] 

        #now lests set times to interpolate to with resolution in CONFIG file, and then interpolate each variable
        new_times = np.linspace(np.nanmin(HOURS_),float(Config_.Forecast_Days*24.),Config_.Interp_Inter*len(HOURS_))
        lat_ = np.interp(new_times,HOURS_,data['lat'][ensno==ii].values)
        lon_ = np.interp(new_times,HOURS_,data['lon'][ensno==ii].values)
        vmx_ = np.interp(new_times,HOURS_,data['vmx'][ensno==ii].values)
        rmw_ = np.interp(new_times,HOURS_,data['rmw'][ensno==ii].values)
        ens_ = np.ones(len(vmx_))*ii

        
        x = new_times
        y = np.interp(new_times,HOURS_,D2L_var[ensno==ii]) #interpolate the distance to land
        dland_dt = np.gradient(y) #calculate the change in distance to land with gradient which conserves shape but is center differentiated
        dland_dt[:-1] = y[1:]-y[:-1] #use a simpler finite differencing alog distance to land


        zero_crossings_arg = np.argwhere(np.diff(np.signbit(y))) #Calculate the index where the distance to land changes sign
        zero_crossings = np.where(np.diff(np.signbit(y)))[0] #gets the value of the zero crossing (output as check)

        for i in np.arange(0,len(zero_crossings)):
            if (dland_dt[zero_crossings_arg[i]]<0):
                #Add in zombie storm check, storms w/ landfalls of 15 kt or higher kept
                if float(vmx_[zero_crossings_arg[i]])<=float(Config_.Minimum_Vmax):
                    continue #skip the next lines and don't count landfall

                land_hour.append(new_times[zero_crossings_arg[i]])
                land_lats.append(lat_[zero_crossings_arg[i]])
                land_lons.append(lon_[zero_crossings_arg[i]])
                try:
                    land_ints.append(np.nanmax(vmx_[int(zero_crossings_arg[i]-int(Config_.Interp_Inter*Config_.Offset_Time)):int(zero_crossings_arg[i]+1)])) #Subtract 6 to get intensity 1 hour prior to landfall
                except: #If we can't get the maximum within an hour, just get the best time
                    land_ints.append(float(vmx_[zero_crossings_arg[i]]))

                land_ensn.append(ens_[zero_crossings_arg[i]])
                land_rmw.append(rmw_[zero_crossings_arg[i]])

                
    return np.vstack((np.squeeze(land_hour), np.squeeze(land_lats), np.squeeze(land_lons), np.squeeze(land_ints),np.squeeze(land_ensn), np.squeeze(land_rmw)))






#------------------#
def wsp_to_SScat(VMX):
    #categorize vmax into a saffir-simpson wind speed category    

    #VMX is an array of intensity values
    #less than 20 is dissipated now
    #TDEPR  34 kt or less
    #TSTOR 35-63 kt
    #Cat 1 64-82 kt
    #Cat 2 83-95 kt
    #Cat 3 96-112 kt
    #Cat 4 113-136 kt
    #Cat 5 137 kt or higher
    
    CAT = np.copy(VMX)

    CAT[(VMX<20)] = -2
    CAT[(VMX>=20)&(VMX<34)]   = -1
    CAT[(VMX>=34)&(VMX<64)]   = 0
    CAT[(VMX>=64)&(VMX<83)]   = 1
    CAT[(VMX>=83)&(VMX<96)]   = 2
    CAT[(VMX>=96)&(VMX<113)]  = 3
    CAT[(VMX>=113)&(VMX<137)] = 4
    CAT[(VMX>=137)] = 5
    
    return CAT

#------------------#

def read_LID_prob(filename):
    '''
    This function will read in the text files created for the Landfall Intensity Distribution. 
    
    Note that <2% will be converted to a -2% for analysis (or other value set in Config file)
    
    '''

    with open(filename, 'r') as fp: #open file

        DATA = [] #define empty list
        REGION = [] #define empty list
        removetable = str.maketrans('<', '-', '') # This converts the < symbol to a negative number
 
        for ind,line in enumerate(fp): #loop through each line
            #Each line will have the same length, so no special logic needed here

            data_line = np.array([x.strip(' ').rstrip() for x in line.split('\t')]) #Strip line to list of strings

            if ind==0: #If this is the first line (i.e. header)
                continue

            elif ind==1: #Second line could also be read in here, but for now just skip it
                continue

            else:
                #For the content of the data file
                data_line = np.array([s.translate(removetable) for s in data_line])
                #Read in the first item on each line which is the region/state
                REGION.append(data_line[0])
                DATA.append((data_line[1:].astype(np.float))) #This is the rest of the data on the line


    return np.vstack((np.squeeze(REGION), np.squeeze(DATA).T))

#------------------#
def get_LID_regions(filename):
    #read in the text file and output the regions with a probability of landfall above threshhold 
    dat = read_LID_prob(filename)
    
    regnames = dat[0,:]  #The first column is the region names
    d5probs = dat[int(Config_.Forecast_Days),:].astype(np.float) #The sixth column is the day 5 probabilities

    return regnames[np.abs(d5probs)>float(Config_.Minimum_Perc)] #only get regions if probability is greater than 2%


#------------------#

def format_LID_output(filename,region,previous=0):
    #This is for reading in the LID files to then use for processing and figure generation

    DTofAn = (filename.split('_')[-2]) #Get the current time
    
    #If a previous (in h) is defined then we substract that amount to get the new filetime 
    T0=dt.datetime.strptime(DTofAn,'%Y%m%d%H')-dt.timedelta(hours=previous)
    T0_string = T0.strftime('%Y%m%d%H')
    filename_ = filename.replace(DTofAn, T0_string, 1)
    
    #This is the times associated with the verification of the daily probabilities
    TIMES = [(T0+dt.timedelta(hours=int(hh))) for hh in np.arange(24,(Config_.Forecast_Days+1)*24,24)]
    
    dat = read_LID_prob(filename_) #read in the datafile
    
    fdat = dat[:,dat[0]==region] #All the data within the matching region
    
    #Group the data into the probabilities over the 5 days
    PROBS = np.abs(np.squeeze(fdat[1:(Config_.Forecast_Days+1)].astype(np.float)))
    PROBSD = np.array([[PROBS[0]]+list(PROBS[1:] - PROBS[:-1])]) #Get the daily probabilities
    
    #format data for other variables
    CATSD = (fdat[(1+Config_.Forecast_Days):(Config_.Forecast_Days+8)].astype(np.float))
    v_90 = np.float(fdat[(Config_.Forecast_Days+8)].astype(np.float))
    v_av = np.float(fdat[(Config_.Forecast_Days+9)].astype(np.float))
    h_10 = np.float(fdat[(Config_.Forecast_Days+10)].astype(np.float))
    h_av = np.float(fdat[(Config_.Forecast_Days+11)].astype(np.float))
    
    #return the data seperated into the times, cumulative probs, daily probs, categories, ...
    return TIMES, PROBS, PROBSD, CATSD, v_90, v_av, h_10, h_av 

 



