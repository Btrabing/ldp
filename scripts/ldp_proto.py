#!/usr/bin/env python
'''
 Written November 10, 2021
 Ben Trabing, btrabing@colostate.edu


This is code has the function to generate a public facing product to highlight the uncertainty associated with the intensity forecasts.
These graphics are prototypes and are liable to change



'''
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
from descartes import PolygonPatch
from matplotlib.collections import LineCollection
import glob
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.geometry import MultiPolygon
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.font_manager import FontProperties
import re
import subprocess
import sys
import xarray as xr
from scipy.interpolate import interp1d
from matplotlib.legend_handler import HandlerPatch


#Inport custom functions
from ldp_main import *
from ldp_util import *
from Plot_util import *

#Hide python warnings
import warnings
warnings.filterwarnings("ignore")

########################################


def roundD(x, base=5):
    #rounds down with an option for the rounding integer value
    return base * np.floor(x/base)

def roundC(x, base=5):
    #rounds up with an option for the rounding integer value
    return base * np.round(x/base)


def roundU(x, base=5):
    #rounds up with an option for the rounding integer value
    return base * np.ceil(x/base)


def flatten(t):
    #removes nested lists
    return [item for sublist in t for item in sublist]



def interp_cubic(x,y,x2):
    f2 = interp1d(x, y, kind='cubic')
    return f2(x2)

def interp_linear(x,y,x2):
    f2 = interp1d(x, y, kind='linear')
    return f2(x2)

#------------------#


def Plot_Ptype_fromtxt(txt_path,mcrall_path=False,GRAPHIC='1',TABLE='1',Output_Loc='./'):
    '''
    Function to create Prototype graphic for Probabilities of landfall and intensity at landfall.

    Required:
    txt_path is the location of the text file output from the LDP
    
    Optional:
    mcrall_path is the location of the mcrall file which has the WSP model tracks
         this path is only required if you want the NHC cone overlaid
    
    
    Graphic 1 is the High-Moderate-Low colored landfall probs
    Graphic 2 is the Greater than 5% colored landfall probs
    
    Table 1 is the prob-<td-ts-hu-mh-90th table
    Table 2 is the simple Most likely and prob-90th table
    
    '''

    #Storm identifier from txt path
    STORMNAME = txt_path.split('_')[-3]
    
    BASIN = STORMNAME[:2] #The basin to get the right config info
    DTofAn = (txt_path.split('_')[-2]) #The datetime (only displayed)

    
    #Read in the distance to land file
    landpath = '../data/gdland_table.dat'
    gdland = read_gdland(landpath)

    #Read in the state geometry and data
    SNAMES,SGEOGS = get_region_shapes(BASIN)
    
    try:
        dat = read_LID_prob(txt_path) #first read in the text file
    except:
        print('Error reading txt file, check if it exists at %s'%txt_path)
        Plot_empty_ldp(mcrall_path,Output_Loc)
        return

    regnames = dat[0,:]  #The first column is the region names
    prob5day = dat[int(Config_.Forecast_Days),:].astype(float) #The sixth column is the day 5 probabilities


    #First lets check if there are any storms with a probability of landfall greather than the minimum
    if (regnames[np.abs(prob5day)>float(Config_.Minimum_Perc)]).size<1:
        #There are no potential landfalls, so plot empty graphic
        Plot_empty_ldp(mcrall_path,Output_Loc)

        return
    
    
    #####################################################    
    #Setup the figure
    tprops=dict(facecolor='white',alpha=1,edgecolor='None')

    print('Plotting Graphic for %s at %s'%(STORMNAME,DTofAn))

    #declare figure (size is slightly different based on size of the table)
    fig, ax, ax3, PROJ = background_map_GT([0.1,359.9],TABLE)


    #This calls the dictionary defining the colors and orders for the states
    mcdict,ordict,abrdict,tzdict=rcdict(BASIN)

    #Add in text descriptors to top of figure
    try:
        ax.text(.01,1.02,get_pubname(STORMNAME.upper()),fontsize=16,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    except:
        ax.text(.01,1.02,STORMNAME.upper(),fontsize=16,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)

    ax.text(.46,1.02,'Experimental',fontsize=16,color='k', bbox=tprops,ha="center",va='bottom',transform=ax.transAxes,zorder=99999)


    ax.text(.99,1.02,'%s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%a %b %d %Y %HZ'),fontsize=16,color='k', bbox=tprops,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)


    #Category names- They are different to include in the table header vs. in the 90th percentile component
    Cat_Names = ['Diss','TD','TS','Cat 1','Cat 2','Cat 3','Cat 4','Cat 5']


    #Define some initial positions
    ii2=0

    cdata_all = [] #saffir simpson prob data to put in table
    lon_wprob = [] #longitude for scatter points with >probability minimum
    lat_wprob = [] #latitude for scatter points with >probability minimum



    #Now loop through the states with landfalls in them
    for idx,state_ in enumerate(regnames):

        #If there is not more than 2% of ensembles with a landfall in the region then skip it
        if prob5day[idx]<=float(Config_.Minimum_Perc): #if there is less than 2% of ensemble members
            continue

        #As long as there is more than the min percentage then plot everyregion in this block
        else:
            
            #Grab the right state geometry
            statel_geom= np.array(SGEOGS)[np.array(SNAMES)==state_]
            
            #Get the defined color and abbreviation for the state name
            if GRAPHIC=='1':
                if prob5day[idx]>50:
                    col='red'
                    zorder_ = 9991113
                    problabel=int(prob5day[idx])
                elif (prob5day[idx]>=25)&(prob5day[idx]<=50):
                    col='orange'
                    zorder_ = 9991112            
                    problabel=int(prob5day[idx])
                elif prob5day[idx]<25:
                    col='yellow'
                    zorder_ = 9991111
                    problabel= int(prob5day[idx])

            elif GRAPHIC=='2':
                if prob5day[idx]>5:
                    col='orangered'
                    zorder_ = 9991113
                    problabel=int(prob5day[idx])


            #This simply gets the abreviation for the region to display in the table (from config file)
            sabr=abrdict[state_]
            
            samp_freq = 20

            #Plot the landfall locations
            try: #if the region is a simple polygon it will be plotted in this section
                x,y = statel_geom.exterior.coords.xy

                x=np.array(x)[::samp_freq]+360
                y=np.array(y)[::samp_freq]
                dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                #save boundaries for determining the bounds of the graphic
                lon_wprob.append(x[dtl_>-25])
                lat_wprob.append(y[dtl_>-25])


            except: #If the region is a multi-polygon (i.e. lots of islands) then they are plotted here
                for geo in statel_geom:
                    try:
                        x, y = geo.exterior.coords.xy
                        x=np.array(x)[::samp_freq]+360
                        y=np.array(y)[::samp_freq]
                        dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                        ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)


                        #save boundaries for determining the bounds of the graphic
                        lon_wprob.append(x[dtl_>-25])
                        lat_wprob.append(y[dtl_>-25])


                    except:
                        for geo_ in geo: #if the polygon within the polygon is a multipolygon it will be plotted here
                            x, y = geo_.exterior.coords.xy
                            x=np.array(x)[::samp_freq]+360
                            y=np.array(y)[::samp_freq]
                            dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                            ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                            #save boundaries for determining the bounds of the graphic
                            lon_wprob.append(x[dtl_>-25])
                            lat_wprob.append(y[dtl_>-25])

            
                            
            #Convert histogram to list of probabilities
            cdata_= list(dat[6:13,idx].astype(float))

            #calculate the 90th percentile intensity and saffir simpson category
            int90 = roundC(1.15078*roundU(dat[13,idx].astype(float))) #converted from kt to mph
            cat90 = np.array(Cat_Names)[int(2+wsp_to_SScat(roundU(dat[13,idx].astype(float))))] 
            int50 = roundC(1.15078*roundU(dat[14,idx].astype(float))) #converted from kt to mph
            cat50 = np.array(Cat_Names)[int(2+wsp_to_SScat(roundU(dat[14,idx].astype(float))))]
            total_hur = np.nansum(np.round_(dat[8:13,idx].astype(float))) #combine probabilities for all hurricanes
            total_maj = np.nansum(np.round_(dat[10:13,idx].astype(float))) #combine probabiliites for all major hurricanes

            # This is a double check to make sure the probabilities add to 100%
            # Only reason it won't add to 100% is because of rounding
            # We round the depression group for simplicity
            cdata_[0] += 100.-(total_hur+int(cdata_[0])+int(cdata_[1]))

            cdata_ = [format("%i%%"%el, '>3') for el in cdata_] #format the probabilities for table


            #Formatting changes- for small and large probababilities show as greater than or less than value
            allprobs = cdata_[0:2]+[format("%i%%"%total_hur, '>3')]+[format("%i%%"%total_maj, '>3')]
            allprobs = ['<2%' if x==' 0%' else x for x in allprobs]
            allprobs = ['<2%' if x==' 1%' else x for x in allprobs]
            allprobs = ['<2%' if x=='-1%' else x for x in allprobs]
            allprobs = ['>98%' if x=='100%' else x for x in allprobs]
            allprobs = ['>98%' if x=='99%' else x for x in allprobs]
            allprobs = ['>98%' if x=='98%' else x for x in allprobs]
    
            problabel_ = [format('>98%','<5') if x>=98. else format('%i%%'%x,'<5') for x in [problabel]]

            
            if TABLE=='1':
                shade_no=6 #shade_no is the index for the table to shade a slightly darker gray
                if ii2==0: #If its the first index and we got this far then:
                    cdata_all.append(['  Region  ']+[' Probability \n of Landfall ']+[' Tropical \n Depression \nor Weaker \n (<39 mph) ']+[' Tropical \n Storm \n (39-73 mph) ']+[' Hurricane \n Cat 1-5 \n (>74 mph) ']+[' Major \n Hurricane \n Cat 3-5 \n (>110 mph) ']+[' Strongest \n Reasonable \n Landfall \n Intensity '])
                    ii2+=1

                cdata_all.append([format("%s"%sabr, '<5')]+problabel_ +allprobs+['%s (%i mph)'%(cat90,int90)])


            if TABLE=='2':
                shade_no=3
                if ii2==0: #If its the first index and we got this far then:
                    cdata_all.append(['  Region  ']+[' Probability\nof Landfall ']+['Most\nLikely\nLandfall\nIntensity']+['Strongest\nReasonable\nLandfall\nIntensity'])
                    ii2+=1

                cdata_all.append([format("%s"%sabr, '<5')]+problabel_ +['%s (%i mph)'%(cat50,int50)]+['%s (%i mph)'%(cat90,int90)])


    if BASIN.upper()=='AL':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-100])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),8])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),54])
    elif BASIN.upper()=='EP':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-120])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),6])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),34])

        #Add Mexico regions in EP
        adm1_shapes = list(shpreader.Reader('../data/All_shp/gadm36_MEX_1.shp').geometries())
        ax.add_geometries(adm1_shapes, ccrs.PlateCarree(),
                  edgecolor='k', facecolor='None', linewidth=.3,zorder=-1)



    #If the plot is too narrow/wide then add some more to the boundaries to reduce that effect
    if ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))<.75:
        urclat=urclat+2
        llclat=llclat-4
    elif ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))>1.25:
        llclon = llclon-3
        urclon = urclon+3

    #Set the map bounds
    ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

    #function to label some regions-modifications made here to map labelling 
    label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ ) #label map regions

    ax3.axis('off') 

    #Plot the table
    #----------------------
    pos1 = ax.get_position() # get the original position 
    

    pos3 = ax3.get_position() # get the original position 
    

    ax3.set_position([pos1.x0+pos1.width+.04, pos3.y0,  pos3.width+.2, pos3.height] )
    tabl=ax3.table(cdata_all,cellLoc='center',bbox=[-.02,-.05,1.5,1.05],loc='center',alpha=1)
#    tabl=ax3.table(cdata_all,cellLoc='center',bbox=[-.18,-.05,1.75,1.05],loc='center',alpha=1)

    tabl.auto_set_font_size(False)
    tabl.set_fontsize(13)

    for i in range(0,len(cdata_all[0])):    
        tabl.auto_set_column_width(i)

    for key, cell in tabl.get_celld().items():
        if (key[1])==shade_no:
            cell.set_facecolor('gray')
        else:
            cell.set_facecolor('silver')

        if (key[1]==0)|(key[0]==0):
            cell.set_text_props(fontproperties=FontProperties(weight='bold'))

        cell.set_linewidth(0.5)
        cell.set_edgecolor('gray')
        cell.set_alpha(.2)


    # Add a table
    ax3.set_title('Tropical Cyclone Intensity Probabilities at Landfall',x=.68,y=1.0,fontsize=18)

    #These are dummy markers added to the plot for the sole purpose of adding a legend
    if GRAPHIC=='1':
        ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='red',label='HIGH  (>50%)')
        ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='orange',label='MODERATE (25%-50%)')
        ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='yellow',label='LOW   (<25%)')
    if GRAPHIC=='2':
        ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='orangered',label='Greater than 5%')


    #add the legend
    leg = ax.legend(loc=3,ncol=1,framealpha=1,prop={'size':10},fancybox=True)
    leg.set_title('Probability of Landfall',prop={'size':11}) #add title to legend
    leg.set_zorder(99999999999999999) #make sure it is above everything else

    ax.add_artist(leg)
    plt.savefig((Output_Loc+'/'+'G%sT%s_2021_Prototype_%s_%s_fromtxt.png'%(GRAPHIC,TABLE,STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)

    if mcrall_path!=False:
        print('Plotting the Cone!')
        Plot_NHC_Cone(mcrall_path,ax,PROJ,5,BASIN.upper())
        get_cone_labels(ax)
        plt.savefig((Output_Loc+'/'+'G%sT%s_2021_Prototype_%s_%s_wcone_fromtxt.png'%(GRAPHIC,TABLE,STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)
        ax3.remove()
        plt.savefig((Output_Loc+'/'+'G%sTNo_2021_Prototype_%s_%s_wcone_fromtxt.png'%(GRAPHIC,STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)



    plt.close(fig)

#    plt.show()



##########################################







#------------------#
def background_map_GT(lon_limits=None,TABLE='1'):
    #This function creates the figure and addes the background map


    HI_RES = '50m'
    MED_RES = '50m'
    PROJ = cartopy.crs.PlateCarree()

    map_proj = ccrs.PlateCarree(central_longitude=np.mean(lon_limits))
    
    if TABLE=='1':
        fig, ax = plt.subplots(
            figsize=(14, 7),
            dpi=200,
            constrained_layout=False,
            facecolor='w',
            edgecolor='k',
            subplot_kw={
                'projection': map_proj,
            },
        )

    elif TABLE=='2':
        fig, ax = plt.subplots(
            figsize=(12, 7),
            dpi=200,
            constrained_layout=False,
            facecolor='w',
            edgecolor='k',
            subplot_kw={
                'projection': map_proj,
            },
        )

    lands = cartopy.feature.NaturalEarthFeature('physical', 'land', HI_RES)
    land_df = pd.DataFrame([[land.area, land] for land in lands.geometries()])
    nlands = len(land_df)
    land_shapes = land_df.sort_values(by=0, axis=0).tail(int(nlands * 0.8))[1]
    lakes = cartopy.feature.NaturalEarthFeature('physical', 'lakes', HI_RES)
    lake_df = pd.DataFrame([[lake.area, lake] for lake in lakes.geometries()])
    lake_shapes = lake_df.sort_values(by=0, axis=0).tail(25)[1]

    ax.background_patch.set_facecolor('lightskyblue')
    # Draw a grey base layer under the continents with the black edge
    great_land_hatching = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor=(0.7, 0.7, 0.7, 0.6),
        zorder=-5,
    )
    ax.add_feature(great_land_hatching)
    great_lands_edges = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor='silver',
        zorder=-1,
    )
    ax.add_feature(great_lands_edges)
    land_shapes = [
        shape.buffer(-0.35) for shape in great_lands_edges.geometries()
    ]
    # Draw the interior white fill with a semi transparent edge    # Draw the interior white fill with a semi transparent edge
    great_land_interior = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor=(0.9, 0.9, 0.9, 0.7),
        linewidth=5,
        facecolor=(1.0, 1.0, 1.0, 0.9),
        zorder=-5,
    )
    ax.add_feature(great_land_interior)
    great_lakes = cartopy.feature.ShapelyFeature(
        lake_shapes,
        crs=PROJ,
        edgecolor='None',
        linewidth=0,
        facecolor='lightskyblue',
        zorder=-1,
    )
    ax.add_feature(great_lakes)
    countries = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_0_boundary_lines_land',
        MED_RES,
        edgecolor='k',
        linewidth=1,
        facecolor='None',
        zorder=-1,
    )
    ax.add_feature(countries)
    borders = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_1_states_provinces_lines',
        MED_RES,
        edgecolor='k',
        linewidth=0.3,
        facecolor='None',
        zorder=-1,
    )
    ax.add_feature(borders)

    
    gs = gridspec.GridSpec(2,3)
    ax.set_position(gs[0:2,0:2].get_position(fig))


    ax2 = fig.add_subplot(gs[:,2])



    #ADD teh grid lines for lat lon
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=1, color='k', alpha=0.8, linestyle='--',zorder=-.5)

    #Specify where to put lat/lon grid lines
    gl.xlocator = mticker.FixedLocator(np.arange(-145,5,5))
    gl.ylocator = mticker.FixedLocator([0,5,10,15,20,25,30,35,40,45,50,55,60])

    #Dont label the right y axis or top of plot
    gl.xlabels_top = False
    gl.ylabels_right = False

    #Make the lat/lon format with E W and N S
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    gl.xlabel_style = {'size': 16, 'color': 'k','weight': 'bold'}
    gl.ylabel_style = {'size': 16, 'color': 'k','weight': 'bold'}

    return fig, ax, ax2, PROJ


#------------------#



def Plot_NHC_Cone(data_filepath,ax,PROJ,ndays,BASIN,plot_centers=False):
    
    plot_lines=False
    
    #--------------------------------------------------------------------------------------------
    
    #Define new set of times to interpolate to
    new_times = np.arange(0,121,1)    
    
    #Error characteristics 
    NHC_Cone_times_=np.array([0,12,24,36,48,60,72,96,120])
    NHC_Cone_times_2=np.array([0,24,48,72,96,120])

#    NHC_Cone_Error=np.array([7.2,  23.7,  36.2,  49.0,  63.8,  83.8,  94.2,  129.8,  172.5]) #in nautical miles
#    NHC_Cone_Error=np.array([11.,  27.,  40.,  55.,  69.,  86.,  102.,  148.,  200.]) #in nautical miles
    NHC_Cone_Error = get_nhc_errors(BASIN) #in n m

    #Interpolate the errors 
    NHC_Cone_Error = interp_linear(NHC_Cone_times_,NHC_Cone_Error,new_times)
    NHC_Cone_times = new_times
    
    #Read in the OFCL forecast which is in the mcrall files
    data = read_ofcl_mcrall(data_filepath)
    
    nhc_times = data['tau'].values
    nhc_lons = data['lon'].values-360.
    nhc_lats = data['lat'].values
    nhc_ints = data['vmx'].values

    nhc_lons[nhc_ints==0]=np.nan
    nhc_lats[nhc_ints==0]=np.nan
    nhc_ints[nhc_ints==0]=np.nan


    ##################################
    #! Center Position Needs to be updated to advisory time
    #! This is a manual update
    #! Future work will need to incorporate additional files to reset this position 
#    nhc_lons[0] = -75.3
#    nhc_lats[0] = 17.9
    #!
    ###################################
     
    #Interpolate to new time resolution
    nhc_lons = interp_linear(nhc_times,nhc_lons,new_times)
    nhc_lats = interp_linear(nhc_times,nhc_lats,new_times)
    nhc_ints = interp_linear(nhc_times,nhc_ints,new_times)

    nhc_times = new_times

    #--------------------------------------------------------------------------------------------


    #Define the circles for the lines
    circs3d = []
    circs5d = []

    #For the 3 day cone
    lons_to_plot3=[]
    lats_to_plot3=[]
    ints_to_plot3=[] #for the symbols

    #for the 5 day cone
    lons_to_plot5=[]
    lats_to_plot5=[]
    ints_to_plot5=[] #for the symbols


    # lets loop through the forecast times
    for i,ntime in enumerate(nhc_times):
        for i2,vtime in enumerate(NHC_Cone_times):
            
            #if the time is less than 72 hours add to the 3 day cone
            if (int(ntime)==int(vtime))&(ntime<=72)&(~np.isnan(nhc_lons[i])):
                lpoint = Point((nhc_lons[i],nhc_lats[i]))
                #! shapely buffer does NOT account for latitudinal differences in great circle distances
                #! This is a temporary fix to improve this issue in the generation of the Cone of Uncertainty
                psize = ((np.sin(np.pi*(nhc_lats[i]-20.)/180.)/2.+1.)*NHC_Cone_Error[i2])/60.0405
                lpoint_buffer = lpoint.buffer(psize)
                lons_to_plot3.append(nhc_lons[i])
                lats_to_plot3.append(nhc_lats[i])
                circs3d.append(lpoint_buffer)

                #determine the symbol for the line based on the intensity (not used right now)
                if nhc_ints[i]*1.15078<39.:
                    ints_to_plot3.append('D')  
                elif nhc_ints[i]*1.15078>110.:
                    ints_to_plot3.append('M')  
                elif ((nhc_ints[i]*1.15078<=110.)&(nhc_ints[i]*1.15078>74.)):
                    ints_to_plot3.append('H')  
                elif ((nhc_ints[i]*1.15078<=74.)&(nhc_ints[i]*1.15078>=39.)):
                    ints_to_plot3.append('S')  
                    
            # if within 5 days add to 5 day cone
            if (int(ntime)==int(vtime))&(ntime<=120)&(~np.isnan(nhc_lons[i])):
                lpoint = Point((nhc_lons[i],nhc_lats[i]))
                #! shapely buffer does NOT account for latitudinal differences in great circle distances
                #! This is a temporary fix to improve this issue in the generation of the Cone of Uncertainty
                psize = ((np.sin(np.pi*(nhc_lats[i]-20.)/180.)/2.+1.)*NHC_Cone_Error[i2])/60.0405
                lpoint_buffer = lpoint.buffer(psize)
                circs5d.append(lpoint_buffer)

                if int(ntime) in NHC_Cone_times_2:
                    lons_to_plot5.append(nhc_lons[i])
                    lats_to_plot5.append(nhc_lats[i])

                    #determine the symbol based on the intensity (not used right now)
                    if nhc_ints[i]*1.15078<39.:
                        ints_to_plot5.append('D')  
                    elif nhc_ints[i]*1.15078>110.:
                        ints_to_plot5.append('M')  
                    elif ((nhc_ints[i]*1.15078<=110.)&(nhc_ints[i]*1.15078>74.)):
                        ints_to_plot5.append('H')  
                    elif ((nhc_ints[i]*1.15078<=74.)&(nhc_ints[i]*1.15078>=39.)):
                        ints_to_plot5.append('S')  


    #--------
    #now we need to create the cones from the circles
    # Day 3
    circs = np.array(circs3d)
    parts1 =[]        
    for first, second in zip(circs, circs[1:]):
        #convex hull of the circles
        parts1.append(first.union(second).convex_hull)

    cone_3day=unary_union(parts1) #join all the circles to create the 3 day cone

    #--------
    #now we need to create the cones from the circles
    # Day 5    
    circs = np.array(circs5d)
    
    parts2 =[]        
    for first, second in zip(circs, circs[1:]):
        #convex hull of the circles
        parts2.append(first.union(second).convex_hull)

    cone_5day=unary_union(parts2) #join all the circles to create the 5 day cone


    #PLOTTING
    #-----------------------------
    if ndays==5: #if we plot the 5 day cone
        differences = cone_5day.difference(cone_3day) #get the difference between teh day 5 and 3
        try:
            for diff in differences: #Sometimes these differences can have really small areas that cause artifacts in plotting
                if diff.area>1: #only plot differences that are large
                    ax.add_geometries([diff],fc='None',ec='w',lw=3,hatch='..',zorder=9991108 ,crs=PROJ)
        except: #iteration will fail if a sinlge polygon is made, so try except is added here
            ax.add_geometries([differences],fc='None',ec='w',lw=3,hatch='..',zorder=9991108 ,crs=PROJ)

        ax.add_geometries([cone_3day],fc='w',ec='k',alpha=.5,lw=2,zorder=9991108 ,crs=PROJ)
        ax.add_geometries([cone_3day],fc='None',ec='k',alpha=1,lw=2,zorder=9991109 ,crs=PROJ) #better define the edges


        #plot the center line
        if plot_lines==True:
            ax.plot(lons_to_plot5,lats_to_plot5,lw=2.5,c='k',transform=PROJ,zorder=9991111 )
        if plot_centers==True:
            for ii in range(0,len(ints_to_plot5)):
                ax.scatter(lons_to_plot5[ii],lats_to_plot5[ii],c='k',s=100,transform=PROJ,zorder=9991111 )
                ax.scatter(lons_to_plot5[ii],lats_to_plot5[ii],marker='$%s$'%ints_to_plot5[ii],c='w',transform=PROJ,zorder=9991111 )



    elif ndays==3:
        ax.add_geometries([cone_3day],fc='w',ec='k',alpha=.5,lw=2,zorder=9991108 ,crs=PROJ)
        ax.add_geometries([cone_3day],fc='None',ec='k',alpha=1,lw=2,zorder=9991109 ,crs=PROJ) #better define the edges

        if plot_lines==True:
            ax.plot(lons_to_plot3,lats_to_plot3,c='k',transform=PROJ,zorder=9991111 )

        for ii in range(0,len(ints_to_plot3)):
            ax.scatter(lons_to_plot3[ii],lats_to_plot3[ii],marker='$%s$'%ints_to_plot3[ii],c='w',transform=PROJ,zorder=9991111 )

#------------------#
def label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ ):
    #Text Color
    Tcolor='dimgray'
    Tsize = 9
    
    
    #------------------
    # First we will label Countries
    
    shpfilename = shpreader.natural_earth(resolution='110m',
                                          category='cultural',
                                          name='admin_0_countries')

    reader = shpreader.Reader(shpfilename)
    countries = reader.records()
    
    pattern = re.compile('[\W_]+')
    

    #Now loop through all the countries
    for country in countries:
        x = country.geometry.centroid.x        
        y = country.geometry.centroid.y
        
        #If the country names have extraneous chaacters or spaces then remove them
        name = pattern.sub('', (country.attributes['NAME']))
        
        #We don't want to label the country since we will label the states in the next loop
        if name=='UnitedStatesofAmerica':
            continue

        #Since Mexico is a large country, we want to add 2 labels, one near the Yucatan and one in
        # the north-central Mexico. This condition adds one both or neither depending on our box
        
        if (name=='Mexico')&(llclat<=15)&(urclat>=26):#&(-105>=llclon)&(-87<=urclon):
            if (-100>=llclon)&(-87<=urclon):
                ax.text(-89.25, 19.7, name, color=Tcolor, size=Tsize, ha='center', va='center', transform=PROJ)
            if (-110>=llclon)&(-87<=urclon):
                ax.text(x, y, name, color=Tcolor, size=Tsize, ha='center', va='center', transform=PROJ)
            
        #Now there are a couple countries where we want to manually move the centriod for labelling 
        #These were set by trial and error and should be updated
        elif ((y>=llclat+.5)&(y<=urclat-.5)&(x>=llclon+2.2)&(x<=urclon-2.2)):
            if name=='Cuba':
                y-=.75
                x-=1
            elif name=='Jamaica':
                y-=.9
            elif name=='PuertoRico':
                y+=.95
            elif name=='Haiti':
                y-=1.5
                x-=.3
            elif name=='DominicanRep':
                y+=.5
                x+=.01
                name='Dominican\nRepublic'


            #now label the centroid of the region
            ax.text(x, y, name, color=Tcolor, size=Tsize, ha='center', va='center', transform=PROJ)

            
            
    #------------------
    # Now we will label the US states
    shpfilename2 = shpreader.natural_earth(resolution='110m',
                                          category='cultural',
                                          name='admin_1_states_provinces_lakes')

    reader = shpreader.Reader(shpfilename2)
    states = reader.records()
    
    #Loop through each US state
    for state in states:
        x = state.geometry.centroid.x        
        y = state.geometry.centroid.y
        

        #If the centroid is within our box
        if ((y>=llclat+.5)&(y<=urclat-.5)&(x>=llclon+1)&(x<=urclon-1)):
            var = state.attributes['postal']
            
            if str(var)=='FL': #shift Florida over
                x+=.5
            elif str(var)=='LA': #shift Louisiana over
                x-=.45             
        
            #Add the label
            ax.text(x, y, var, color=Tcolor, size=Tsize, ha='center', va='center', transform=PROJ,zorder=999999999)

    
    
#------------------#
    

def get_pubname(IDNAME):
    '''
    Get the Storm name given the TC identifier
    Input: 'AL102021'--> Output: 'Kate'

    The path_to_storm_file needs to be set:
    Currently set to look for real-time storm_list.txt within the atcf
    ------------------------------------------------#
    '''    
    # Set the path of the storm_list file (found in atcf/index)
#    path_to_storm_file = '../data/storm_list.txt'
    path_to_storm_file = '/atcf/index/storm_list.txt'
#    path_to_storm_file = '../data/master_list.txt'

    
    #read in the storm list file
    data = np.genfromtxt(path_to_storm_file, dtype=str,delimiter=',',autostrip=True)
    
    #use the name/identifier to create a dictionary
    STORMNAME_DICT = dict(zip(data[:,-1],data[:,0]))


    
    return STORMNAME_DICT[IDNAME.upper()] #return dictionary value


#------------------#

def test_cone_graphic(filename):
    '''
    This Function will plot and label an NHC forecast given a mcrall file

    Should only be used to test changes to the cone element on the LDP graphics
    '''
    fig, ax, ax2, PROJ = background_map_GT([0,359.9])

    #Set a boundary: may want to update depending on case
    llclon=-120
    llclat=6
    urclon=-80
    urclat=45.8
    #Set the boundaries
    ax.set_extent([llclon, urclon, llclat, urclat], crs=PROJ)

    # Plot the cone provided in the mcrall file
    Plot_NHC_Cone(filename,ax,PROJ,5,'AL')
    
    #Label the map regions
    label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ )


    plt.show()






class PolygonN(HandlerPatch):
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height
        aspect= height/float(width)
        verts = orig_handle.get_xy()
        minx, miny = verts[:,0].min(), verts[:,1].min()
        maxx, maxy = verts[:,0].max(), verts[:,1].max()
        aspect= (maxy-miny)/float((maxx-minx))
        nvx = (verts[:,0]-minx)*float(height)/aspect/(maxx-minx)-x0
        nvy = (verts[:,1]-miny)*float(height)/(maxy-miny)-y0

        p = mpatches.Polygon(np.c_[nvx, nvy])
        p.update_from(orig_handle)
        p.set_transform(handlebox.get_transform())

        handlebox.add_artist(p)
        return p



def get_cone_labels(ax):
    
    #custom shape 1
    c3d = Polygon([[p.x, p.y] for p in [Point(-.2,0), Point(-1,-.3), Point(-1,.3), Point(-.2,0)]]).union(Point(-1,0).buffer(.3))

    #create custom shape 2
    start = (Point(1,1).buffer(1))
    for i in np.arange(0,.5,.05):
        start = start.union((Point(1+i*4,1).buffer(1-i/2.)))

    #final custom shape 2
    c5d = start.difference(Point(3.1,1).buffer(.8))
    
    legend_els = [mpatches.Polygon(np.squeeze(c3d.exterior.xy).T,closed=True,facecolor='white',lw=3, edgecolor='k',label='Day 1-3'),
                       mpatches.Polygon(np.squeeze(c5d.exterior.xy).T,closed=True,facecolor='white',edgecolor='k',lw=3,hatch='..',label='Day 3-5')]

    leg2 = ax.legend(handles=legend_els,loc=4,handleheight=3, handlelength=4, framealpha=1,fancybox=True,fontsize=9,handler_map={mpatches.Polygon: PolygonN()})
    leg2.set_title('Potential Track Area',prop={'size':9}) #add title to legend
    leg2.set_zorder(99999999999999999)




def OFCL_Intensity_(data_filepath,gdland):
    '''
    This function takes the mcrall filepath and a distance to land field and outputs a corrected
    and interpolated intensity forecast from NHC. The intensity forecast is corrected at objectively
    defined landfalls to reduce intensity bias when landfall occurs between synoptic times.
    


    Usage: Fhour, INTS= OFCL_Intensity_('/Users/btrabing/ldp/data/mcrall_al142018_101006.dat',gdland)


    '''


    inter_hours = 1  # hourly interpolation resolution


    #--------------------------------------------------------------------------------------------

    #Define new set of times to interpolate to
    new_times = np.arange(0,24*Config_.Forecast_Days+inter_hours,inter_hours)


    #Read in the OFCL forecast which is in the mcrall files
    data = read_ofcl_mcrall(data_filepath)

    nhc_times = data['tau'].values
    nhc_lons = data['lon'].values
    nhc_lats = data['lat'].values
    nhc_ints = data['vmx'].values

    #Interpolate to new time resolution
    new_nhc_lons = interp_linear(nhc_times,nhc_lons,new_times)
    new_nhc_lats = interp_linear(nhc_times,nhc_lats,new_times)
    new_nhc_d2l  = is_land(new_nhc_lons,new_nhc_lats,gdland)
    new_nhc_ints = interp_linear(nhc_times,nhc_ints,new_times)


    #check for landfalls
    dland_dt = np.gradient(new_nhc_d2l) #calculate the change in distance to land with gradient which conserves shape but is center differentiated
    dland_dt[:-1] = new_nhc_d2l[1:]-new_nhc_d2l[:-1] #use a simpler finite differencing alog distance to land

    #The landfall index
    land_indexes = np.argwhere(np.diff(np.signbit(new_nhc_d2l))) #Calculate the index where the distance to land changes sign

    # If there are no landfalls, return the uncorrected intensity forecast
    if len(land_indexes)<1:
        return new_times, new_nhc_ints


    old_bounds = []
    New_ints = np.copy(new_nhc_ints)

    for land_index in land_indexes:
        #Get the indexes corresonding to the 6/12 hour forecast intervals prior to and after landfall
        prev_12  = int(roundD(land_index-6,6))
        prev_6   = int(roundD(land_index,6))
        next_6   = int(roundU(land_index,6))
        next_12  = int(roundU(land_index+6,6))
        ref_ints = [prev_12, prev_6, next_6, next_12]

        #Now we want to get the forecast hour that is closest to just before and after the landfall
        #Loop through reference intensities at the synopt times to get the best ones
        index_ = 0 #used to determine the first and second choice
        for ref_i in ref_ints:

            if (ref_i in nhc_times)&(index_==0):
                bound_0 = ref_i
                index_ =1
            elif (ref_i in nhc_times)&(index_==1):
                bound_1 = ref_i
                index_ =2

        # Check if we already have corrected a similar landfall time, if so skip
        if bound_0 in old_bounds:
            continue


        Int_before = New_ints[bound_0]

        try: #check that the slope is not undefined, if so then the landfall is at a synoptic time 
            Slope = float(New_ints[bound_1]-Int_before)/float(bound_1-land_index)
        except: #no correction needed so skip
            continue

        # If the intensity is increasing, then don't correct downwards
        if Slope>0:
            continue

        #Now loop through the intensity time period and correct the intensity
        for i in range(bound_0,bound_1,inter_hours):
            # Prior to the landfall, we will extrapolate the previous overwater forecast
            if i<= int(land_index):
                New_ints[i] = Int_before
            # after landfall, use a simple linear interpolation
            elif  i > int(land_index):
                New_ints[i] = Slope*(i-land_index)+Int_before


        old_bounds.append(bound_0)


    return new_times, New_ints




def Plot_VMAX90_fromtxt(txt_path,mcrall_path=False,Output_Loc='./'):
    '''
    This function uses the LDP output to show a plot of the Resonable maximum intensity at landfalls plotted on a map


    Required:
    txt_path is the location of the text file output from the LDP
    
    Optional:
    mcrall_path is the location of the mcrall file which has the WSP model tracks
         this path is only required if you want the NHC cone overlaid
    
    '''

    #Storm identifier from txt path
    STORMNAME = txt_path.split('_')[-3]
    
    BASIN = STORMNAME[:2] #The basin to get the right config info
    DTofAn = (txt_path.split('_')[-2]) #The datetime (only displayed)

    
    #Read in the distance to land file
    landpath = '../data/gdland_table.dat'
    gdland = read_gdland(landpath)

    #Read in the state geometry and data
    SNAMES,SGEOGS = get_region_shapes(BASIN)
    
    try:
        dat = read_LID_prob(txt_path) #first read in the text file
    except:
        print('Error reading txt file, check if it exists at %s'%txt_path)
        return


    regnames = dat[0,:]  #The first column is the region names
    prob5day = dat[int(Config_.Forecast_Days),:].astype(float) #The sixth column is the day 5 probabilities


    #First lets check if there are any storms with a probability of landfall greather than the minimum
    if (regnames[np.abs(prob5day)>float(Config_.Minimum_Perc)]).size<1:
        #There are no potential landfalls, so do nothing
        return
    

    #This calls the dictionary defining the colors and orders for the states
    mcdict,ordict,abrdict,tzdict=rcdict(BASIN)
    
    #####################################################    
    #Setup the figure
    tprops=dict(facecolor='white',alpha=1,edgecolor='None')

    print('Plotting VMAX Graphic for %s at %s'%(STORMNAME,DTofAn))

    #declare figure (size is slightly different based on size of the table)
    fig, ax, ax3, PROJ = background_map_GT([0.1,359.9],'1')
    ax3.remove() #We are not plotting the table here

    #Add in text descriptors to top of figure
    try:
        ax.text(.01,1.02,get_pubname(STORMNAME.upper()),fontsize=12,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    except:
        ax.text(.01,1.02,STORMNAME.upper(),fontsize=12,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)

    ax.text(.46,1.02,'Experimental',fontsize=10,color='k', bbox=tprops,ha="center",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.99,1.02,'%s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%a %b %d %Y %HZ'),fontsize=12,color='k', bbox=tprops,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)



    #Define some initial positions
    ii2=0

    lon_wprob = [] #longitude for scatter points with >probability minimum
    lat_wprob = [] #latitude for scatter points with >probability minimum


    #Now loop through the states with landfalls in them
    for idx,state_ in enumerate(regnames):

        #If there is not more than 2% of ensembles with a landfall in the region then skip it
        if prob5day[idx]<=float(Config_.Minimum_Perc): #if there is less than 2% of ensemble members
            continue

        #As long as there is more than the min percentage then plot everyregion in this block
        else:
            
            #Grab the right state geometry
            statel_geom= np.array(SGEOGS)[np.array(SNAMES)==state_]
            
            
            #calculate the 90th percentile intensity and saffir simpson category
            int90 = roundC(roundU(dat[13,idx].astype(float))) #in kt

            if prob5day[idx]>=25.:

                if int90>=137:
                    col='magenta'
                    zorder_ = 9991113

                elif (int90<137)&(int90>=113):
                    col='maroon'
                    zorder_ = 9991112

                elif (int90<113)&(int90>=96):
                    col='red'
                    zorder_ = 9991112
                elif (int90<96)&(int90>=83):
                    col='orange'
                    zorder_ = 9991112
                elif (int90<83)&(int90>=64):
                    col='gold'
                    zorder_ = 9991112
                elif (int90<64)&(int90>=34):
                    col = 'green'
                    zorder_ = 9991111
                elif (int90<34):
                    col = 'blue'
                    zorder_ = 9991111

            elif prob5day[idx]<25:
                col = 'cyan'
                zorder_ = 9991110


            samp_freq = 20

            #Plot the landfall locations
            try: #if the region is a simple polygon it will be plotted in this section
                x,y = statel_geom.exterior.coords.xy

                x=np.array(x)[::samp_freq]+360
                y=np.array(y)[::samp_freq]
                dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                #save boundaries for determining the bounds of the graphic
                lon_wprob.append(x[dtl_>-25])
                lat_wprob.append(y[dtl_>-25])


            except: #If the region is a multi-polygon (i.e. lots of islands) then they are plotted here
                for geo in statel_geom:
                    try:
                        x, y = geo.exterior.coords.xy
                        x=np.array(x)[::samp_freq]+360
                        y=np.array(y)[::samp_freq]
                        dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                        ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)


                        #save boundaries for determining the bounds of the graphic
                        lon_wprob.append(x[dtl_>-25])
                        lat_wprob.append(y[dtl_>-25])


                    except:
                        for geo_ in geo: #if the polygon within the polygon is a multipolygon it will be plotted here
                            x, y = geo_.exterior.coords.xy
                            x=np.array(x)[::samp_freq]+360
                            y=np.array(y)[::samp_freq]
                            dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                            ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                            #save boundaries for determining the bounds of the graphic
                            lon_wprob.append(x[dtl_>-25])
                            lat_wprob.append(y[dtl_>-25])

            
                            

    if BASIN.upper()=='AL':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-100])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),8])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),54])
    elif BASIN.upper()=='EP':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-120])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),6])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),34])

        #Add Mexico regions in EP
        adm1_shapes = list(shpreader.Reader('../data/All_shp/gadm36_MEX_1.shp').geometries())
        ax.add_geometries(adm1_shapes, ccrs.PlateCarree(),
                  edgecolor='k', facecolor='None', linewidth=.3,zorder=-1)




    #If the plot is too narrow/wide then add some more to the boundaries to reduce that effect
    if ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))<.75:
        urclat=urclat+2
        llclat=llclat-4
    elif ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))>1.25:
        llclon = llclon-3
        urclon = urclon+3

    #Set the map bounds
    ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

    #function to label some regions-modifications made here to map labelling 
    label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ ) #label map regions


    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='magenta',label='Category 5 Hurricane (>157 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='maroon',label='Category 4 Hurricane (130-156 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='red',label='Category 3 Hurricane (111-129 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='orange',label='Category 2 Hurricane (96-110 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='gold',label='Category 1 Hurricane (74-95 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='green',label='Tropical Storm (39-73 mph) ')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='blue',label='Tropical Depression or Weaker (<39 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='cyan',label='Low Probability of Landfall (<25%)')



    #add the legend
    leg = ax.legend(loc=3,ncol=1,framealpha=1,prop={'size':8},fancybox=True)
    leg.set_title('Strongest Reasonable Intensity\n          If Landfall Occurs  ',prop={'size':11}) #add title to legend
    leg.set_zorder(99999999999999999)


    ax.add_artist(leg)
    plt.savefig((Output_Loc+'/'+'VMAX_90_v3_Prototype_%s_%s_fromtxt.png'%(STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)

    if mcrall_path!=False:
        print('Plotting the Cone!')
        Plot_NHC_Cone(mcrall_path,ax,PROJ,5,BASIN.upper())
        get_cone_labels(ax)
        plt.savefig((Output_Loc+'/'+'VMAX_90_v3_Prototype_%s_%s_wcone_fromtxt.png'%(STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)


    plt.close(fig)

    
    
def Plot_VMAX50_fromtxt(txt_path,mcrall_path=False,Output_Loc='./'):
    '''
    This function will create a graphic of just the most likely landfall intensity

    Required:
    txt_path is the location of the text file output from the LDP
    
    Optional:
    mcrall_path is the location of the mcrall file which has the WSP model tracks
         this path is only required if you want the NHC cone overlaid
    
    '''

    #Storm identifier from txt path
    STORMNAME = txt_path.split('_')[-3]
    
    BASIN = STORMNAME[:2] #The basin to get the right config info
    DTofAn = (txt_path.split('_')[-2]) #The datetime (only displayed)

    
    #Read in the distance to land file
    landpath = '../data/gdland_table.dat'
    gdland = read_gdland(landpath)

    #Read in the state geometry and data
    SNAMES,SGEOGS = get_region_shapes(BASIN)
    
    try:
        dat = read_LID_prob(txt_path) #first read in the text file
    except:
        print('Error reading txt file, check if it exists at %s'%txt_path)
        return


    regnames = dat[0,:]  #The first column is the region names
    prob5day = dat[int(Config_.Forecast_Days),:].astype(float) #The sixth column is the day 5 probabilities


    #First lets check if there are any storms with a probability of landfall greather than the minimum
    if (regnames[np.abs(prob5day)>float(Config_.Minimum_Perc)]).size<1:
        #There are no potential landfalls, so do nothing
        return
    

    #This calls the dictionary defining the colors and orders for the states
    mcdict,ordict,abrdict,tzdict=rcdict(BASIN)
    
    #####################################################    
    #Setup the figure
    tprops=dict(facecolor='white',alpha=1,edgecolor='None')

    print('Plotting VMAX Graphic for %s at %s'%(STORMNAME,DTofAn))

    #declare figure (size is slightly different based on size of the table)
    fig, ax, ax3, PROJ = background_map_GT([0.1,359.9],'1')
    ax3.remove() #We are not plotting the table here

    #Add in text descriptors to top of figure
    try:
        ax.text(.01,1.02,get_pubname(STORMNAME.upper()),fontsize=12,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    except:
        ax.text(.01,1.02,STORMNAME.upper(),fontsize=12,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)

    ax.text(.46,1.02,'Experimental',fontsize=10,color='k', bbox=tprops,ha="center",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.99,1.02,'%s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%a %b %d %Y %HZ'),fontsize=12,color='k', bbox=tprops,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)



    #Define some initial positions
    ii2=0

    lon_wprob = [] #longitude for scatter points with >probability minimum
    lat_wprob = [] #latitude for scatter points with >probability minimum


    #Now loop through the states with landfalls in them
    for idx,state_ in enumerate(regnames):

        #If there is not more than 2% of ensembles with a landfall in the region then skip it
        if prob5day[idx]<=float(Config_.Minimum_Perc): #if there is less than 2% of ensemble members
            continue

        #As long as there is more than the min percentage then plot everyregion in this block
        else:
            
            #Grab the right state geometry
            statel_geom= np.array(SGEOGS)[np.array(SNAMES)==state_]
            
            
            #calculate the 90th percentile intensity and saffir simpson category
            int50 = roundC(roundU(dat[14,idx].astype(float))) #converted nearest 5 kt

            if prob5day[idx]>=25.:

                if int50>=137:
                    col='magenta'
                    zorder_ = 9991113

                elif (int50<137)&(int50>=113):
                    col='maroon'
                    zorder_ = 9991112

                elif (int50<113)&(int50>=96):
                    col='red'
                    zorder_ = 9991112
                elif (int50<96)&(int50>=83):
                    col='orange'
                    zorder_ = 9991112
                elif (int50<83)&(int50>=64):
                    col='gold'
                    zorder_ = 9991112
                elif (int50<64)&(int50>=34):
                    col = 'green'
                    zorder_ = 9991111
                elif (int50<34):
                    col = 'blue'
                    zorder_ = 9991111

            elif prob5day[idx]<25:
                col = 'cyan'
                zorder_ = 9991110


            samp_freq = 50

            #Plot the landfall locations
            try: #if the region is a simple polygon it will be plotted in this section
                x,y = statel_geom.exterior.coords.xy

                x=np.array(x)[::samp_freq]+360
                y=np.array(y)[::samp_freq]
                dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                #save boundaries for determining the bounds of the graphic
                lon_wprob.append(x[dtl_>-25])
                lat_wprob.append(y[dtl_>-25])


            except: #If the region is a multi-polygon (i.e. lots of islands) then they are plotted here
                for geo in statel_geom:
                    try:
                        x, y = geo.exterior.coords.xy
                        x=np.array(x)[::samp_freq]+360
                        y=np.array(y)[::samp_freq]
                        dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                        ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)


                        #save boundaries for determining the bounds of the graphic
                        lon_wprob.append(x[dtl_>-25])
                        lat_wprob.append(y[dtl_>-25])


                    except:
                        for geo_ in geo: #if the polygon within the polygon is a multipolygon it will be plotted here
                            x, y = geo_.exterior.coords.xy
                            x=np.array(x)[::samp_freq]+360
                            y=np.array(y)[::samp_freq]
                            dtl_ = is_land(x,y,gdland) #use the distance to land to only add hatching to coastal regions
                            ax.scatter(x[dtl_>-25]-360.,y[dtl_>-25],transform=PROJ,marker='o',linewidth=0,facecolor='None',ec=col,hatch='xxxxxxx', s=50,alpha=1,zorder=zorder_)

                            #save boundaries for determining the bounds of the graphic
                            lon_wprob.append(x[dtl_>-25])
                            lat_wprob.append(y[dtl_>-25])

            
                            


    if BASIN.upper()=='AL':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-100])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),8])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),54])
    elif BASIN.upper()=='EP':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array(flatten(lon_wprob))-360.)-4.5,2),-120])
        llclat=np.nanmax([roundD(np.nanmin(np.array(flatten(lat_wprob)))-6,2),6])
        urclon=np.nanmin([roundU(np.nanmax(np.array(flatten(lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array(flatten(lat_wprob)))+2,2),36])

        #Add Mexico regions in EP
        adm1_shapes = list(shpreader.Reader('../data/All_shp/gadm36_MEX_1.shp').geometries())
        ax.add_geometries(adm1_shapes, ccrs.PlateCarree(),
                  edgecolor='k', facecolor='None', linewidth=.3,zorder=-1)




    #If the plot is too narrow/wide then add some more to the boundaries to reduce that effect
    if ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))<.75:
        urclat=urclat+2
        llclat=llclat-4
    elif ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))>1.25:
        llclon = llclon-3
        urclon = urclon+3

    #Set the map bounds
    ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

    #function to label some regions-modifications made here to map labelling 
    label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ ) #label map regions


    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='magenta',label='Category 5 Hurricane (>157 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='maroon',label='Category 4 Hurricane (130-156 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='red',label='Category 3 Hurricane (111-129 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='orange',label='Category 2 Hurricane (96-110 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='gold',label='Category 1 Hurricane (74-95 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='green',label='Tropical Storm (39-73 mph) ')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='blue',label='Tropical Depression or Weaker (<39 mph)')
    ax.scatter([0],[-300],s=100,marker='s',c='None',fc='None',linewidth=0,hatch='xxxxxxx',ec='cyan',label='Low Probability of Landfall (<25%)')



    #add the legend
    leg = ax.legend(loc=3,ncol=1,framealpha=1,prop={'size':8},fancybox=True)
    leg.set_title('Most Likely Intensity\n  If Landfall Occurs ',prop={'size':11}) #add title to legend

    leg.set_zorder(99999999999999999)


    ax.add_artist(leg)
    plt.savefig((Output_Loc+'/'+'VMAX_50_v3_Prototype_%s_%s_fromtxt.png'%(STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)

    if mcrall_path!=False:
        print('Plotting the Cone!')
        Plot_NHC_Cone(mcrall_path,ax,PROJ,5,BASIN.upper())
        get_cone_labels(ax)
        plt.savefig((Output_Loc+'/'+'VMAX_50_v3_Prototype_%s_%s_wcone_fromtxt.png'%(STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)


    plt.close(fig)


def Plot_empty_ldp(mcrall_path,Output_Loc='./'):
    '''
    This function will plot just the cone of uncertainty
    '''

    #Storm identifier from txt path
    STORMNAME = mcrall_path.split('_')[-2].upper()

    BASIN = STORMNAME[:2] #The basin to get the right config info
    DTofAn = (STORMNAME[-4:]+mcrall_path.split('_')[-1][:-4]) #The datetime (only displayed)

    #Read in the OFCL forecast which is in the mcrall files
    data = read_ofcl_mcrall(mcrall_path)

    nhc_times = data['tau'].values
    nhc_lons = data['lon'].values
    nhc_lats = data['lat'].values
    nhc_ints = data['vmx'].values

    #####################################################    
    #Setup the figure
    tprops=dict(facecolor='white',alpha=1,edgecolor='None')

    print('Plotting No-Landfall Graphic for %s at %s'%(STORMNAME,DTofAn))

    #declare figure (size is slightly different based on size of the table)
    fig, ax, ax3, PROJ = background_map_GT([0.1,359.9],'2')

    #Add in text descriptors to top of figure
    try:
        ax.text(.01,1.02,get_pubname(STORMNAME.upper()),fontsize=16,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    except:
        ax.text(.01,1.02,STORMNAME.upper(),fontsize=16,color='k', bbox=tprops,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)

    ax.text(.46,1.02,'Experimental',fontsize=14,color='k', bbox=tprops,ha="center",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.99,1.02,'%s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%a %b %d %Y %HZ'),fontsize=16,color='k', bbox=tprops,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)


    #Category names- They are different to include in the table header vs. in the 90th percentile component
    Cat_Names = ['Diss','TD','TS','Cat 1','Cat 2','Cat 3','Cat 4','Cat 5']

    HOURS_to_show=[0,24,48,72,96,120]

    #The data to include int eh table
    cdata_all = [] #saffir simpson prob data to put in table
    cdata_all.append(['  Time  ']+['Most\nLikely\nIntensity'])

    lon_wprob = nhc_lons #longitude for scatter points with >probability minimum
    lat_wprob = nhc_lats #latitude for scatter points with >probability minimum
    for ii2 in range(0,len(nhc_times)):
        if int(nhc_times[ii2]) in HOURS_to_show:
            int50 = roundC(1.15078*roundU(nhc_ints[ii2].astype(float))) #converted from kt to mph
            cat50 = np.array(Cat_Names)[int(2+wsp_to_SScat(roundU(nhc_ints[ii2].astype(float))))]

            cdata_all.append(["%i"%int(nhc_times[ii2])]+['%s (%i mph)'%(cat50,int50)])


    if BASIN.upper()=='AL':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array((lon_wprob))-360.)-4.5,2),-100])
        llclat=np.nanmax([roundD(np.nanmin(np.array((lat_wprob)))-6,2),8])
        urclon=np.nanmin([roundU(np.nanmax(np.array((lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array((lat_wprob)))+2,2),54])
    elif BASIN.upper()=='EP':
        #Set the boundaries of the plot with some rounding, a buffer, and a minimum domain
        llclon=np.nanmax([roundD(np.nanmin(np.array((lon_wprob))-360.)-4.5,2),-120])
        llclat=np.nanmax([roundD(np.nanmin(np.array((lat_wprob)))-6,2),6])
        urclon=np.nanmin([roundU(np.nanmax(np.array((lon_wprob))-360.)+4.5,2),-20])
        urclat=np.nanmin([roundU(np.nanmax(np.array((lat_wprob)))+2,2),34])


    #If the plot is too narrow/wide then add some more to the boundaries to reduce that effect
    if ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))<.75:
        urclat=urclat+2
        llclat=llclat-4
    elif ((np.abs(urclat-llclat)/np.abs(urclon-llclon)))>1.25:
        llclon = llclon-3
        urclon = urclon+3

    #Set the map bounds
    ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

    #function to label some regions-modifications made here to map labelling 
    label_map_regions(ax,llclon, urclon, llclat, urclat,PROJ ) #label map regions

    ax3.axis('off')

    #Plot the table
    #----------------------
    pos1 = ax.get_position() # get the original position 
    pos3 = ax3.get_position() # get the original position 

    ax3.set_position([pos1.x0+pos1.width+.04, pos3.y0,  pos3.width+.2, pos3.height] )
    tabl=ax3.table(cdata_all,cellLoc='center',bbox=[0.,0.05,.8,.8],loc='center',alpha=1)

    tabl.auto_set_font_size(False)
    tabl.set_fontsize(13)

    for i in range(0,len(cdata_all[0])):
        tabl.auto_set_column_width(i)

    for key, cell in tabl.get_celld().items():
        cell.set_facecolor('silver')

        cell.set_text_props(fontproperties=FontProperties(weight='bold'))

        cell.set_linewidth(0.5)
        cell.set_edgecolor('gray')
        cell.set_alpha(.2)

    # Add a table
    ax3.set_title('Tropical Cyclone Intensity Forecast',x=.4,y=.9,fontsize=16)

    Plot_NHC_Cone(mcrall_path,ax,PROJ,5,BASIN.upper(),True)
    get_cone_labels(ax)
    plt.savefig((Output_Loc+'/'+'G1TNo_2021_Prototype_%s_%s_wcone_fromtxt.png'%(STORMNAME.upper(),DTofAn)),bbox_inches='tight',pad_inches=0.1)

    plt.close(fig)









if __name__ =='__main__':

#    test_cone_graphic('/nhc_home/btrabing/mcrall/2021/mcrall_ep172021_102312.dat')

    Plot_Ptype_fromtxt('../output/WSPLF_AL092021_2021082612_Probs.txt',mcrall_path='../data/mcrall/mcrall_al092021_082612.dat',GRAPHIC='1',TABLE='2',Output_Loc='../output')









