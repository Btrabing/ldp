#!/usr/bin/env python
#------------------------------
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
import matplotlib.path as mpath
import matplotlib.dates as md
from matplotlib.dates import DateFormatter
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
from dateutil import tz

from ldp_util import *

#------------------#
def rcdict(BASIN=False):

    if BASIN==False:
        # This function outputs the dictionaries in the text file
        # Note that is for older version, and not a combined basin landfall product    
        modelc = np.genfromtxt('../config/Region_cparams.txt', dtype=str,delimiter=',',skip_header=1)
    elif BASIN.upper()=='AL':
        # This function outputs the dictionaries in the text file    
        modelc = np.genfromtxt('../config/AL_cparams.txt', dtype=str,delimiter=',',skip_header=1)
    elif BASIN.upper()=='EP':
        # This function outputs the dictionaries in the text file    
        modelc = np.genfromtxt('../config/EP_cparams.txt', dtype=str,delimiter=',',skip_header=1)

    mcdict = {} #dictrionary for colors
    mmdict = {} #dictionarry for name ordering
    abrdict = {} #dictionary for region abbreviation
    tzdict = {}  #dictionary for specific timezone

    for line in modelc[:]:
        mcdict[line[0]]=line[1].strip()
        mmdict[line[0]]=int(line[2].strip())
        abrdict[line[0]]=line[3].strip()
        tzdict[line[0]]=line[5].strip()

    return mcdict, mmdict, abrdict, tzdict


#------------------#
def get_region_shapes(BASIN='AL'):
    #Using import cartopy.io.shapereader as shpreader
    try:
        reader = shpreader.Reader(("../data/%s_SHP_Regions/%s_SHP_Regions.shp"%(BASIN.upper(),BASIN.upper())),encoding="ISO-8859-1")
    except:
        print('Modification to shapereader needed')
        print('See: https://github.com/SciTools/cartopy/issues/1282')
        print('Remove the try to pinpoint location of file on compute system')
        return

    name = []
    geos  = []

    data = reader.records()
    for cc in data:
        name.append(cc.attributes['Name'])
        geos.append(cc.geometry)

    return name, geos
#------------------#

def rorder_states(state_list,BASIN=False):
    
    mcdict,ordict,abrdict,tzdict = rcdict(BASIN)
    sort_args = []
    snames_args = []
    
    for Rname in state_list:
        snames_args.append(Rname)
        sort_args.append(ordict[Rname])

    
    return np.array(snames_args)[np.argsort(sort_args)]
#------------------#

def format_to_localtime(dtofan,hour_,TIMEZONE):
    #Requires:
    #from dateutil import tz
    #import datetime as dt
    
    #If you want the current time in a timezone then set hour_=0


    to_zone = tz.gettz(TIMEZONE) #Get the timezone info from location string
    
    if isinstance(dtofan,dt.datetime): #if we already have a datetime object
        enddt = dtofan+dt.timedelta(hours=hour_)
        return (enddt.astimezone(to_zone).strftime('%A\n%I%p %Z'))
    else: #If we don't have a datetime object, then we assume the format is a string
        enddt = dt.datetime.strptime(dtofan,'%Y%m%d%H') #convert to datetime object
        enddt = enddt.replace(tzinfo=dt.timezone.utc)+dt.timedelta(hours=hour_) #set timezone and add hours
        return (enddt.astimezone(to_zone).strftime('%A\n%I%p %Z')) #Convert to new time zone and convert to str

#------------------#

def array_to_localtime(tarray,TIMEZONE,form=False):
    #Requires:
    #from dateutil import tz
    #import datetime as dt
    
    #If form is provided, it should be a string for datetime conversion such as '%Y%m%d:%H%M %z'
    
    to_zone = tz.gettz(TIMEZONE) #Get the timezone info from location string
        
    if form!=False:
        new_list = [(dtof.astimezone(to_zone)).strftime(form) for dtof in tarray]
    else:
        new_list = [(dtof.astimezone(to_zone)) for dtof in tarray]
    
    return np.array(new_list)



#------------------#
def format_vsub(ax2,n_clusters_):
    #Adds formatting to subplot for intensity distributions
    ax2.set_xticks(np.arange(0,180,20))

    #Alternating shading of saffir simpson wind hurricane scale
    ax2.fill_betweenx(np.arange(0,300,10),0,34,color='silver',zorder=1,alpha=.3)
    ax2.fill_betweenx(np.arange(0,300,10),64,83,color='silver',zorder=1,alpha=.3)
    ax2.fill_betweenx(np.arange(0,300,10),96,113,color='silver',zorder=1,alpha=.3)
    ax2.fill_betweenx(np.arange(0,300,10),137,190,color='silver',zorder=1,alpha=.3)

    ax2.set_xlabel('Intensity at Landfall (kt)')

    #Increases size depending on the number of potential regions with landfalls
    ax2.set_ylim(0,10*(n_clusters_))

    #Max intensity is set at 160 kt (may need to change)
    ax2.set_xlim(0,160)

    #Don't include any information about sample size in plot
    ax2.set_yticks([])

#------------------#

def format_hsub(ax3,n_clusters_):
    #Formats the subplot of time

    ax3.set_xticks(np.arange(0,Config_.Forecast_Days*24+12,12))

    #alternating background shading every 24h
    ax3.fill_betweenx(np.arange(0,300,10),24,48,color='silver',zorder=1,alpha=.3)
    ax3.fill_betweenx(np.arange(0,300,10),72,96,color='silver',zorder=1,alpha=.3)
    ax3.fill_betweenx(np.arange(0,300,10),120,144,color='silver',zorder=1,alpha=.3)

    #If we have more forecast time then add in extra shading
    if int(Config_.Forecast_Days)>5:
        ax3.fill_betweenx(np.arange(0,300,10),168,192,color='silver',zorder=1,alpha=.3)

    ax3.set_xlabel('Time of Landfall (h)')

    #set plot limits
    ax3.set_ylim(0,10*(n_clusters_))
    ax3.set_xlim(0,Config_.Forecast_Days*24)

    ax3.set_yticks([]) #don't show ticks

#------------------#
    
def format_sssub(ax2,n_clusters_):
    # sets format of saffir-simpson distribution
    # Note currently unused

    ax2.set_xticks(np.arange(-2,6,1))
    ax2.set_xticklabels(['Diss','TD','TS','H1','H2','H3','H4','H5'])

    ax2.set_ylim(0,10*(n_clusters_))

    #Shading for hurricanes
    ax2.fill_betweenx(np.arange(0,300,10),.5,7,color='silver',zorder=1,alpha=.3)
    #Increase shading for major hurricanes
    ax2.fill_betweenx(np.arange(0,300,10),2.5,7,color='silver',zorder=1,alpha=.3)


    ax2.set_xlim(-2.5,5.5)

    ax2.set_yticks([])


#------------------#





#------------------#
def background_plot_map(lon_limits=None,BASIN='AL'):
    #This function creates the figure and addes the background map
    

    HI_RES = '50m'
    MED_RES = '50m'
    PROJ = cartopy.crs.PlateCarree()

    map_proj = ccrs.PlateCarree(central_longitude=np.mean(lon_limits))

    fig, ax = plt.subplots(
        figsize=(13, 7),
        dpi=400,
        constrained_layout=False,
        facecolor='w',
        edgecolor='k',
        subplot_kw={
            'projection': map_proj,
        },
    )

    lands = cartopy.feature.NaturalEarthFeature('physical', 'land', HI_RES)
    land_df = pd.DataFrame([[land.area, land] for land in lands.geometries()])
    nlands = len(land_df)
    land_shapes = land_df.sort_values(by=0, axis=0).tail(int(nlands * 0.8))[1]
    lakes = cartopy.feature.NaturalEarthFeature('physical', 'lakes', HI_RES)
    lake_df = pd.DataFrame([[lake.area, lake] for lake in lakes.geometries()])
    lake_shapes = lake_df.sort_values(by=0, axis=0).tail(25)[1]

    ax.background_patch.set_facecolor('None')
    # Draw a grey base layer under the continents with the black edge
    great_land_hatching = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor=(0.7, 0.7, 0.7, 0.6),
        zorder=900,
    )
    ax.add_feature(great_land_hatching)
    great_lands_edges = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor='None',
        zorder=10999,
    )
    ax.add_feature(great_lands_edges)
    land_shapes = [
        shape.buffer(-0.35) for shape in great_lands_edges.geometries()
    ]
    # Draw the interior white fill with a semi transparent edge
    great_land_interior = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor=(0.9, 0.9, 0.9, 0.7),
        linewidth=5,
        facecolor=(1.0, 1.0, 1.0, 0.9),
        zorder=901,
    )
    ax.add_feature(great_land_interior)
    great_lakes = cartopy.feature.ShapelyFeature(
        lake_shapes,
        crs=PROJ,
        edgecolor='None',
        linewidth=0,
        facecolor='None',
        zorder=902,
    )
    ax.add_feature(great_lakes)
    countries = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_0_boundary_lines_land',
        MED_RES,
        edgecolor='k',
        linewidth=1,
        facecolor='None',
        zorder=10091,
    )
    ax.add_feature(countries)
    borders = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_1_states_provinces_lines',
        MED_RES,
        edgecolor='k',
        linewidth=0.3,
        facecolor='None',
        zorder=10091,
    )
    ax.add_feature(borders)

   

    gs = gridspec.GridSpec(2,3)
    ax.set_position(gs[0:2,0:2].get_position(fig))
    #ax.set_subplotspec(gs[0:2,0:2])

    ax2 = fig.add_subplot(gs[0,2])
    ax3 = fig.add_subplot(gs[1,2])

    ############
    # WHich basin to show
    if BASIN==False:
        ############
        llclon=-104.01
        llclat=18.9
        urclon=-60.99
        urclat=55.01

    elif BASIN.upper()=='AL':
        ############
        llclon=-99.01
        llclat=8.9
        urclon=-51.99
        urclat=54.01
    elif BASIN.upper()=='EP':
        ############
        llclon=-119.01
        llclat=3
        urclon=-74.99
        urclat=43.01
    elif BASIN.upper()=='BOTH':
        ############
        llclon=-119.01
        llclat=3
        urclon=-51.99
        urclat=54.01        


    #Set the boundaries of the map
    ax.set_extent([llclon, urclon, llclat, urclat], crs=ccrs.PlateCarree())

    #ADD teh grid lines for lat lon
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=1, color=(0.7, 0.7, 0.7, 0.5), alpha=0.5, linestyle='--',zorder=1002)

    #Specify where to put lat/lon grid lines
    gl.xlocator = mticker.FixedLocator(np.arange(-145,5,5))
    gl.ylocator = mticker.FixedLocator([0,5,10,15,20,25,30,35,40,45,50,55,60])

    #Dont label the right y axis or top of plot
    gl.xlabels_top = False
    gl.ylabels_right = False

    #Make the lat/lon format with E W and N S
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER


    return fig, ax, ax2, ax3, PROJ


#------------------#

def background_plot_map_AEA(lon_limits=None):
    #This code uses a different map projection and will need additional testing/logic to implement

    HI_RES = '50m'
    MED_RES = '50m'

    ############
    llclon=-100.01
    llclat=17.9
    urclon=-60.99
    urclat=50.01
    ############


    PROJ = cartopy.crs.PlateCarree()

    proj = ccrs.AlbersEqualArea(central_longitude=(llclon+urclon)/2.,
                                central_latitude=(llclat+urclat)/2.,
                                standard_parallels=(10, 50))

    #fig.canvas.draw()
    fig, ax = plt.subplots(
        figsize=(15, 7),
        dpi=400,
        constrained_layout=True,
        facecolor='w',
        edgecolor='k',
        subplot_kw={
            'projection': proj,
        },
    )



    ax = plt.axes(projection=proj)  


    ax.set_extent([llclon, urclon, llclat, urclat], crs=PROJ)


    lands = cartopy.feature.NaturalEarthFeature('physical', 'land', HI_RES)
    land_df = pd.DataFrame([[land.area, land] for land in lands.geometries()])
    nlands = len(land_df)

    land_shapes = land_df.sort_values(by=0, axis=0).tail(int(nlands * 0.8))[1]
    lakes = cartopy.feature.NaturalEarthFeature('physical', 'lakes', HI_RES)
    lake_df = pd.DataFrame([[lake.area, lake] for lake in lakes.geometries()])
    lake_shapes = lake_df.sort_values(by=0, axis=0).tail(25)[1]

    ax.background_patch.set_facecolor('None')

    great_land_hatching = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor=(0.7, 0.7, 0.7, 0.6),
        zorder=900,
    )
    ax.add_feature(great_land_hatching)

    great_lands_edges = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor='k',
        linewidth=1.,
        facecolor='None',
        zorder=10999,
    )
    ax.add_feature(great_lands_edges)

    land_shapes = [
        shape.buffer(-0.35) for shape in great_lands_edges.geometries()
    ]
    # Draw the interior white fill with a semi transparent edge
    great_land_interior = cartopy.feature.ShapelyFeature(
        land_shapes,
        crs=PROJ,
        edgecolor=(0.9, 0.9, 0.9, 0.7),
        linewidth=5,
        facecolor=(1.0, 1.0, 1.0, 0.9),
        zorder=901,
    )
    ax.add_feature(great_land_interior)

    great_lakes = cartopy.feature.ShapelyFeature(
        lake_shapes,
        crs=PROJ,
        edgecolor='None',
        linewidth=0,
        facecolor='None',
        zorder=902,
    )
    ax.add_feature(great_lakes)

    countries = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_0_boundary_lines_land',
        MED_RES,
        edgecolor='k',
        linewidth=1,
        facecolor='None',
        zorder=10091,
    )
    ax.add_feature(countries)

    borders = cartopy.feature.NaturalEarthFeature(
        'cultural',
        'admin_1_states_provinces_lines',
        MED_RES,
        edgecolor='k',
        linewidth=0.3,
        facecolor='None',
        zorder=10091,
    )
    ax.add_feature(borders)




    #gl = ax.gridlines(crs=ccrs.PlateCarree(), linewidth=2, color='black', alpha=0.5, linestyle='--', draw_labels=False)

    gs = gridspec.GridSpec(2,3)
    ax.set_position(gs[0:2,0:2].get_position(fig))
    #ax.set_subplotspec(gs[0:2,0:2])


    ax2 = fig.add_subplot(gs[0,2])
    ax3 = fig.add_subplot(gs[1,2])



    # Make a boundary path in PlateCarree projection, I choose to start in
    # the bottom left and go round anticlockwise, creating a boundary point
    # every 1 degree so that the result is smooth:
    vertices = [(lon, int(llclat)) for lon in range(int(llclon), int(urclon), 1)] + [(lon, int(urclat)) for lon in range(int(urclon), int(llclon), -1)]
    boundary = mpath.Path(vertices)
    ax.set_boundary(boundary, transform=ccrs.PlateCarree())


    gl = ax.gridlines(crs=PROJ,linewidth=1,draw_labels=False, color=(0.7, 0.7, 0.7, 0.5), 
                      alpha=0.5, linestyle='--',zorder=1002)

    #Specify where to put lat/lon grid lines
    gl.xlocator = mticker.FixedLocator(np.arange(-145,5,5))
    gl.ylocator = mticker.FixedLocator([0,5,10,15,20,25,30,35,40,45,50,55,60])

    t1props=dict(facecolor='none',alpha=1,edgecolor='None')
    tprops=dict(facecolor='none',alpha=1,edgecolor='None')
    ax.text(-.03,.1,'20$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.0,.25,'25$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.03,.4,'30$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.06,.55,'35$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.09,.70,'40$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.12,.85,'45$^\circ$N',fontsize=12,color='k', bbox=t1props,ha="right",va='center',transform=ax.transAxes,zorder=99999)


    ax.text(.11,-.05,'95$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.24,-.06,'90$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.38,-.07,'85$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.52,-.075,'80$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.65,-.07,'75$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.78,-.06,'70$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)
    ax.text(.92,-.05,'65$^\circ$W',fontsize=12,color='k', bbox=t1props,ha="center",va='center',transform=ax.transAxes,zorder=99999)



    #Dont label the right y axis or top of plot
    gl.xlabels_top = False
    gl.ylabels_right = False

    #Make the lat/lon format with E W and N S
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    return fig, ax, ax2, ax3, PROJ






