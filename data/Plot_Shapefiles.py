#!/usr/bin/env python
# coding: utf-8
'''
This code provides functions to read in, plot, modify, write, and then read and plot again

Converted from jupyter notebook to script (so not optimized and will probably take a long time to run

Written by Ben Trabing, CIRA/NHC August 27 2021
Contact Ben.Trabing@noaa.gov


'''


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.ticker
from descartes import PolygonPatch

import glob
import matplotlib.patheffects as PathEffects
import matplotlib.text as mtext
import matplotlib.ticker as mticker
import matplotlib.transforms as mtrans
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches
from cartopy.mpl import gridliner
from matplotlib.patches import Ellipse
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.ops import unary_union
import cartopy.io.shapereader as shpreader
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import geopandas as gpd
from shapely import geometry
import xarray as xr

#---------------------------------------------------------------------------#

#Read in a file, to show kind of output
shapefile = gpd.read_file("All_shp/gadm36_MEX_1.shp")

#Print out the names of the variables within the shapefile
for header in (shapefile):
    print(header)



#---------------------------------------------------------------------------#
# ## Function to cut small islands and display shapefile

def cut_region(filename):
    #
    # This code will cut out polygons smaller than `minsize`
    #
    
    minsize=.05
    
    #First read in the shapefile
    shapefile = gpd.read_file(filename)
    reso = int(filename.split('_')[-1][:1])



    names_to_save = []
    geom_to_save = []
    
    
    for i,mpoly in enumerate(shapefile['geometry']):
        Reg = shapefile['NAME_%i'%reso][i]


    
        try: #If there is multipolygon (i.e. when there are islands)
            temp_geom_list =[]


            for poly in mpoly:     #Loop through the islands       
                if poly.area<minsize: #If there is a small area then keep going
                    pass
                else:
                    temp_geom_list.append(poly) #if the size is larger than minsize then add to list
                    
            #After looping through polygons, combine them and save to list
            geom_to_save.append(unary_union(temp_geom_list))
            names_to_save.append(Reg)

        except: #if we have just one polygon then no need to loop through polygons
            if mpoly.area<minsize: #If the polygon is too small
                pass
            else: #If the size is larger than minsize then save to output
                geom_to_save.append(mpoly)
                names_to_save.append(Reg)    
                
    return names_to_save,geom_to_save
    
    
def plot_shape_(filename,cut=False):    
    #Create figure
    PROJ = cartopy.crs.PlateCarree()
    fig = plt.figure(figsize=(14,10))

    ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)
    
    lowerlon=[]
    lowerlat=[]
    upperlon=[]
    upperlat=[]
    
   
    if isinstance(filename, list):
        
        for filename_ in filename:
        

            #First read in the shapefile
            shapefile = gpd.read_file(filename_)
            reso = int(filename_.split('_')[-1][:1])

            #If cut is true then we will call the function to remove some of the smaller polygons
            if cut==True:
                NAMES,GEOMETRIES = cut_region(filename_)
            else:        
                GEOMETRIES = shapefile['geometry']
                NAMES = shapefile['NAME_%i'%reso]

            for i,mpoly in enumerate(GEOMETRIES):
                Reg = NAMES[i]

                try:
                    for poly in mpoly:            
                        patch1 = PolygonPatch(poly,transform=PROJ,fc='C0')
                        ax.add_patch(patch1)        

                        lowerlon.append(np.nanmin(poly.exterior.xy[0]))
                        lowerlat.append(np.nanmin(poly.exterior.xy[1]))
                        upperlon.append(np.nanmax(poly.exterior.xy[0]))
                        upperlat.append(np.nanmax(poly.exterior.xy[1]))

                except:
                    patch1 = PolygonPatch(mpoly,transform=PROJ,fc='C0')
                    ax.add_patch(patch1)        

                    lowerlon.append(np.nanmin(mpoly.exterior.xy[0]))
                    lowerlat.append(np.nanmin(mpoly.exterior.xy[1]))
                    upperlon.append(np.nanmax(mpoly.exterior.xy[0]))
                    upperlat.append(np.nanmax(mpoly.exterior.xy[1]))

                point_coords = mpoly.centroid.coords[0]

                ax.text(point_coords[0],point_coords[1],NAMES[i],transform=PROJ,horizontalalignment='center',zorder=3)


    else:                

        #First read in the shapefile
        shapefile = gpd.read_file(filename)
        reso = int(filename.split('_')[-1][:1])

        #If cut is true then we will call the function to remove some of the smaller polygons
        if cut==True:
            NAMES,GEOMETRIES = cut_region(filename)
        else:        
            GEOMETRIES = shapefile['geometry']
            NAMES = shapefile['NAME_%i'%reso]

        for i,mpoly in enumerate(GEOMETRIES):
            Reg = NAMES[i]

            try:
                for poly in mpoly:            
                    patch1 = PolygonPatch(poly,transform=PROJ,fc='C0')
                    ax.add_patch(patch1)        

                    lowerlon.append(np.nanmin(poly.exterior.xy[0]))
                    lowerlat.append(np.nanmin(poly.exterior.xy[1]))
                    upperlon.append(np.nanmax(poly.exterior.xy[0]))
                    upperlat.append(np.nanmax(poly.exterior.xy[1]))

            except:
                patch1 = PolygonPatch(mpoly,transform=PROJ,fc='C0')
                ax.add_patch(patch1)        

                lowerlon.append(np.nanmin(mpoly.exterior.xy[0]))
                lowerlat.append(np.nanmin(mpoly.exterior.xy[1]))
                upperlon.append(np.nanmax(mpoly.exterior.xy[0]))
                upperlat.append(np.nanmax(mpoly.exterior.xy[1]))

            point_coords = mpoly.centroid.coords[0]

            ax.text(point_coords[0],point_coords[1],NAMES[i],transform=PROJ,horizontalalignment='center',zorder=3)

              
    ax.set_extent([np.nanmin(lowerlon)-1, np.nanmax(upperlon)+1, np.nanmin(lowerlat)-1, np.nanmax(upperlat)+1], PROJ)


    plt.show()
    

#---------------------------------------------------------------------------#
# ## Example of Before and After small polygons removed


#Plot Carribean Islands with small polygons removed
plot_shape_(["All_shp/gadm36_CUB_0.shp","All_shp/gadm36_HTI_0.shp","All_shp/gadm36_DOM_0.shp","All_shp/gadm36_JAM_0.shp","All_shp/gadm36_PRI_0.shp"],True)

#Plot Carribean Islands without small polygons removed
plot_shape_(["All_shp/gadm36_CUB_0.shp","All_shp/gadm36_HTI_0.shp","All_shp/gadm36_DOM_0.shp","All_shp/gadm36_JAM_0.shp","All_shp/gadm36_PRI_0.shp"],False)


#---------------------------------------------------------------------------#
# ## Use functions to split regions in some states (plot to verify)


#These 3 functions define the 3 regions of Florida
def get_FLpanh(lat,lon,var):
    return var[lon<=-83.3]
    
def get_FLwest(lat,lon,var):
    return var[((lat)>25.0)&((lon)>-83.3)&((((lon)<-82)&(lat>=28))|(((lon)<-80.9)&(lat<28)))]

def get_FLeast(lat,lon,var):
    return var[(((lon)>-82)&(lat>=28))|(((lon)>-80.9)&(lat<28)&(lon<-79.99))]

def get_TXwest(lat,lon,var):
    return var[((lat<=28.5)&(lon>-100.5))] #Note some of inland texas removed

def get_TXeast(lat,lon,var):
    return var[((lat>28.5)&(lat<32)&(lon>-101))] #Note some of inland texas removed
#------------------#

def plot_florida_regions():
    #
    # Readin the USA data, remove small islands, split polygons, and plot
    #

    names,geos = cut_region("All_shp/gadm36_USA_1.shp")  

    for i,name in enumerate(names):
        if name=='Florida':

            points=np.array(geos[i].exterior.coords[:-1])



            region_1 = get_FLpanh(points[:,1],points[:,0],points)
            region_2 = get_FLwest(points[:,1],points[:,0],points)
            region_3 = get_FLeast(points[:,1],points[:,0],points)


            poly1 = geometry.Polygon([[p[0], p[1]] for p in region_1])
            poly2 = geometry.Polygon([[p[0], p[1]] for p in region_2])
            poly3 = geometry.Polygon([[p[0], p[1]] for p in region_3])


            PROJ = cartopy.crs.PlateCarree()

            fig = plt.figure(figsize=(8,8))
            ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)

            colors_ = ['b','g','r']
            for col,poly in zip(colors_,[poly1,poly2,poly3]):

                patch1 = PolygonPatch(poly,fc=col,  ec='k',transform=PROJ, alpha=0.95, zorder=2)
                ax.add_patch(patch1)  

            ax.set_extent([-88, -78.9, 23, 33], PROJ)


            plt.show()        


def plot_texas_regions():
    #
    # Readin the USA data, remove small islands, split polygons, and plot
    #

    names,geos = cut_region("All_shp/gadm36_USA_1.shp")  

    for i,name in enumerate(names):
        if name=='Texas':

            points=np.array(geos[i].exterior.coords[:-1])



            region_1 = get_TXeast(points[:,1],points[:,0],points)
            region_2 = get_TXwest(points[:,1],points[:,0],points)
#            region_3 = get_FLeast(points[:,1],points[:,0],points)


            poly1 = geometry.Polygon([[p[0], p[1]] for p in region_1])
            poly2 = geometry.Polygon([[p[0], p[1]] for p in region_2])
     
        
#            poly3 = geometry.Polygon([[p[0], p[1]] for p in region_3])


            PROJ = cartopy.crs.PlateCarree()

            fig = plt.figure(figsize=(8,8))
            ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)

            colors_ = ['C0','C3']
            for col,poly in zip(colors_,[poly1,poly2]):
                

                patch1 = PolygonPatch(poly,fc=col,  ec='k',transform=PROJ, alpha=0.95, zorder=2)
                ax.add_patch(patch1)  

            ax.set_extent([-110, -90, 23, 43], PROJ)


            plt.show()        



plot_florida_regions()

plot_texas_regions()


#---------------------------------------------------------------------------#
# ## The regions that will be used for the Landfall Intensity Distributions


names_to_save_EP= ['Honduras','Nicaragua','Baja California','Baja California Sur','Guatemala', 
       'Chiapas', 'Colima', 'Guerrero', 'Jalisco', 'Michoacán', 'Nayarit', 'Oaxaca', 'Sonora', 
       'Sinaloa', 'Panama', 'El Salvador', 'Costa Rica', 'Hawaii', 'California']

names_to_save_AL= ['New Brunswick', 'Nova Scotia', 'Prince Edward Island', 'Honduras', 'Nicaragua', 'Belize', 
                   'Puerto Rico', 'Guatemala', 'Campeche', 'Quintana Roo', 'Tabasco', 'Tamaulipas', 'Veracruz', 
                   'Yucatán', 'Jamaica', 'Panama','Alabama', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 
                   'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Mississippi', 'New Hampshire', 
                   'New Jersey', 'New York', 'North Carolina','Pennsylvania', 'Rhode Island', 'South Carolina', 
                   'Texas', 'Virginia', 'Costa Rica', 'Haiti','Dominican Republic', 'Cuba']


#---------------------------------------------------------------------------#
# ## Plot the maps of the data with the Shapefiles we will use



PROJ = cartopy.crs.PlateCarree()

fig = plt.figure(figsize=(12,10))
ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)




for filename in glob.glob('All_shp/*.shp'):    
    names,geos = cut_region(filename)      

    for i,poly in enumerate(geos):   
        
        Reg = names[i]
        
        if Reg in names_to_save_AL:
            patch1 = PolygonPatch(poly,fc='r',  ec='k',transform=PROJ, alpha=0.55, zorder=2)
            ax.add_patch(patch1)
        else:
            patch1 = PolygonPatch(poly,fc='C0',  ec='k',transform=PROJ, alpha=0.35, zorder=2)
            ax.add_patch(patch1)  
            
            
            

ax.set_extent([-115, -40.5, 7, 51], PROJ)

ax.tick_params(axis='y')
ax.tick_params(axis='x')

plt.show()
        

#
#
# Now Plot the East Pacific Region
#

PROJ = cartopy.crs.PlateCarree()

fig = plt.figure(figsize=(12,10))
ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)





for filename in glob.glob('All_shp/*.shp'):   
    
    names,geos = cut_region(filename)  


    

    for i,poly in enumerate(geos):   
        
        Reg = names[i]
        
        if Reg in names_to_save_EP:
            patch1 = PolygonPatch(poly,fc='r',  ec='k',transform=PROJ, alpha=0.55, zorder=2)
            ax.add_patch(patch1)
        else:
            patch1 = PolygonPatch(poly,fc='C0',  ec='k',transform=PROJ, alpha=0.35, zorder=2)
            ax.add_patch(patch1)  
            
            
            

ax.set_extent([-162, -78.5, 6, 45], PROJ)


plt.show()
        


#---------------------------------------------------------------------------#
# ## Save polygons to output Shapefile



#
# Loop through regions in shapefile and copy names/geometries to a list
#
# Combine arrays and save to pandas, then to geopandas before saving shapefile output
#

geom_ep = []
name_ep = []


for filename in glob.glob('All_shp/*.shp'):    
    names,geos = cut_region(filename)  


    for i,poly in enumerate(geos):   
        
        Reg = names[i]
        
        if Reg in names_to_save_EP:
            
            print(Reg)
            geom_ep.append(poly)
            name_ep.append(Reg)


data_ep = np.concatenate((np.array(name_ep).reshape(-1,1),np.squeeze(geom_ep).reshape(-1,1)),axis=1)

df_ep = pd.DataFrame(data_ep,columns=['Name','geometry'])   

gdf = gpd.GeoDataFrame(df_ep)
gdf.to_file('EP_SHP_Regions')



#
# Loop through regions in shapefile and copy names/geometries to a list
#
# Combine arrays and save to pandas, then to geopandas before saving shapefile output
#

geom_al = []
name_al = []


for filename in glob.glob('All_shp/*.shp'):    
    names,geos = cut_region(filename)  


    for i,poly in enumerate(geos):   
        
        Reg = names[i]
        
        if Reg in names_to_save_AL:
            
            if Reg=='Florida':
                points=np.array(poly.exterior.coords[:-1])

                region_1 = get_FLpanh(points[:,1],points[:,0],points)
                region_2 = get_FLwest(points[:,1],points[:,0],points)
                region_3 = get_FLeast(points[:,1],points[:,0],points)


                poly1 = geometry.Polygon([[p[0], p[1]] for p in region_1])
                poly2 = geometry.Polygon([[p[0], p[1]] for p in region_2])
                poly3 = geometry.Polygon([[p[0], p[1]] for p in region_3])
                
                
                geom_al.append(poly1)
                name_al.append('FL Pan')
                
                geom_al.append(poly2)
                name_al.append('West FL')

                geom_al.append(poly3)
                name_al.append('East FL')
                
                print('FL Pan')
                print('West FL')
                print('East FL')


            elif Reg=='Texas':
                points=np.array(poly.exterior.coords[:-1])

                region_1 = get_TXeast(points[:,1],points[:,0],points)
                region_2 = get_TXwest(points[:,1],points[:,0],points)

                poly1 = geometry.Polygon([[p[0], p[1]] for p in region_1])
                poly2 = geometry.Polygon([[p[0], p[1]] for p in region_2])

                geom_al.append(poly1)
                name_al.append('East TX')
                
                geom_al.append(poly2)
                name_al.append('West TX')

                print('East TX')
                print('West TX')

                
            else: 
                print(Reg)
                geom_al.append(poly)
                name_al.append(Reg)


data_al = np.concatenate((np.array(name_al).reshape(-1,1),np.squeeze(geom_al).reshape(-1,1)),axis=1)

df_al = pd.DataFrame(data_al,columns=['Name','geometry'])   

gdfa = gpd.GeoDataFrame(df_al)
gdfa.to_file('AL_SHP_Regions')


#---------------------------------------------------------------------------#
# ## Output Tables


# Copy and paste for table template required for new config file in the Atlantic
for i,val,in enumerate(names_to_save_AL): 
    print(format(val+',','<27'),'ACR,',' 20,','color,')
    



# Copy and paste for table template required for new config file in the Atlantic
for i,val,in enumerate(names_to_save_EP): 
    print(format(val+',','<27'),'ACR,',' 20,','color,')


#---------------------------------------------------------------------------#
# ## Reader for new shapefiles

def get_regions(BASIN='AL'):
    #Using import cartopy.io.shapereader as shpreader

    reader = shpreader.Reader(("%s_SHP_Regions/%s_SHP_Regions.shp"%(BASIN.upper(),BASIN.upper())))
    
    name = []
    geos  = []
    
    data = reader.records()
    for cc in data:
        name.append(cc.attributes['Name'])
        geos.append(cc.geometry)
        
    return name, geos


    
get_regions()




def get_regions(BASIN='AL'):
    #Using import geopandas as gdp
    
    shapefile = gpd.read_file(("%s_SHP_Regions/%s_SHP_Regions.shp"%(BASIN.upper(),BASIN.upper())))
      
    GEOMETRIES = shapefile['geometry'].values
    NAMES = shapefile['Name'].values
    
    return list(NAMES), list(GEOMETRIES)

get_regions()


#---------------------------------------------------------------------------#
## Display Just the Coastal regions we will use


def plot_cust_shape(BASIN='AL'):
    filename = ("%s_SHP_Regions/%s_SHP_Regions.shp"%(BASIN.upper(),BASIN.upper()))
    #Create figure
    PROJ = cartopy.crs.PlateCarree()
    fig = plt.figure(figsize=(14,10))

    ax = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree(),frameon=False)
    
    lowerlon=[]
    lowerlat=[]
    upperlon=[]
    upperlat=[]
    
             

    #First read in the shapefile
    shapefile = gpd.read_file(filename)

    #If cut is true then we will call the function to remove some of the smaller polygons
      
    GEOMETRIES = shapefile['geometry']
    NAMES = shapefile['Name']

    for i,mpoly in enumerate(GEOMETRIES):
        Reg = NAMES[i]

        try:
            for poly in mpoly:            
                patch1 = PolygonPatch(poly,transform=PROJ,fc='C0')
                ax.add_patch(patch1)        

                lowerlon.append(np.nanmin(poly.exterior.xy[0]))
                lowerlat.append(np.nanmin(poly.exterior.xy[1]))
                upperlon.append(np.nanmax(poly.exterior.xy[0]))
                upperlat.append(np.nanmax(poly.exterior.xy[1]))

        except:
            patch1 = PolygonPatch(mpoly,transform=PROJ,fc='C0')
            ax.add_patch(patch1)        

            lowerlon.append(np.nanmin(mpoly.exterior.xy[0]))
            lowerlat.append(np.nanmin(mpoly.exterior.xy[1]))
            upperlon.append(np.nanmax(mpoly.exterior.xy[0]))
            upperlat.append(np.nanmax(mpoly.exterior.xy[1]))

        point_coords = mpoly.centroid.coords[0]

        ax.text(point_coords[0],point_coords[1],NAMES[i],transform=PROJ,horizontalalignment='center',zorder=3)

              
    ax.set_extent([np.nanmin(lowerlon)-1, np.nanmax(upperlon)+1, np.nanmin(lowerlat)-1, np.nanmax(upperlat)+1], PROJ)


    plt.show()
    
    


#plot Atlantic Regions
plot_cust_shape('AL')


#plot EP Regions
plot_cust_shape('EP')



