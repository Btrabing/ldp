# Landfall Distribution Product (LDP)

**Landfall-Distribution-Product (LDP)**\
Benjamin Trabing\
CIRA/CSU/NHC\
Ben.Trabing@noaa.gov

## Overview

The landfall Distribution Product (LDP) includes code to create probabilities of landfall within specified regions and to display of the probabilities within a graphic. The code uses the Monte-Carlo Wind Speed Probability (WSP) model output to first calculate the probability of landfall within each of the statistical realizations. The WSP model is a statistical ensemble based on the error characteristics of forecasts by the National Hurricane Center (NHC) and the spread of climatological models, which is currently used at NHC. An important benefit of using the WSP model in deriving this product is that the landfall probabilities will provide consistent messaging with other operational products from the NHC. The LDP is currently running internally at NHC and work is ongoing to improve the methods and graphical displays.


To calculate the landfall and intensity probabilities, the LDP uses a distance to land file to calculate where each statistical ensemble member makes landfall, then categorizes that landfall based on geographic area. To do this categorization, the LDP uses regional shapefiles of state, island, and country boundaries. Some of these shapefiles have been manually modified to create sub regions (i.e. splitting up the coastline of Florida). The accumulation of the statistical realizations are then used to produce estimates of the probability of landfall and conditional intensity probabilities in the regions where landfall is forecast to occur. 

## Running the LDP

### Getting Started
To download the code:

    git clone git@gitlab.com:Btrabing/ldp.git
 
### Code Requirements

Requires the following python packages. Run each of the following to check:

    import shapely
    import descartes
    import cartopy 
    import pandas as pd
    import numpy as np
    import xarray as xr
    import matplotlib

The code utilizes the python subprocess library and calls bash commands. You may need to install bash if running on a windows machine. 


There is a bug in the Cartopy code that will need to be updated because of a utf-8 decoding error. If newer versions of Cartopy do not correct this, then follow the instructions here: https://github.com/SciTools/cartopy/issues/1282


In order to run the LDP, you will need mcrall files from the WSP model. An example file is provided in `ldp/data/mcrall`. All other data files needed to run the LDP are found within `ldp/data`.

### How to Run


To run the LDP and create the figures first modify `scripts/run_ldp.py`

Within `run_ldp.py`, you will need to update the `MCRALL_Path` and specify `STMID` to run the storm you are interested in. Then simply:

    python run_ldp.py


## Updating the Shapefiles

In order to update shapefiles you will need to install `geopandas`. Within `ldp/data/Plot_Shapefiles.py` there are functions to read in the region shapefiles and cut, plot, and reconfigure the regions used. Note that when shapefiles are split, the config files will need to be updated. 


## Relevant Resources and Presentations
See https://ams.confex.com/ams/102ANNUAL/meetingapp.cgi/Paper/394454 \
See https://ams.confex.com/ams/35Hurricanes/meetingapp.cgi/Paper/402057

A manuscript documenting the LDP with a detailed verification has been published in Weather and Forecasting at https://journals.ametsoc.org/view/journals/wefo/38/8/WAF-D-22-0199.1.xml

If code is used from this project, please cite this project using:

Trabing, B. C., K. D. Musgrave, M. DeMaria, B. C. Zachry, M. J. Brennan, and E. N. Rappaport, 2023: The Development and Evaluation of a Tropical Cyclone Probabilistic Landfall Forecast Product. Wea. Forecasting, 38, 1363–1374, https://doi.org/10.1175/WAF-D-22-0199.1.


